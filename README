Gimp Color Management Module

This module is gcmm, the Gimp Color Management Module. It is derived
from code written orignially by "ciccio" given away as free software
to Olof Kylander, and is now being actively enhanced by Gimp
developers, most notably Olof, Raph Levien, and Jay Cox. Be sure to
check the ChangeLog for the complete list.

The goal of gcmm is to provide a high quality, free software
implementation of color management, comparable in scope to Kodak's
KCMS and Apple's ColorSync. Like those systems, gcmm is based on
standard ICC profiles. At a minimum, gcmm will support input profiles
for scanners, display profiles for monitors, and printer profiles for
offset printing and a wide range of color printers, including the
popular inkjets.

The default colorspace for gcmm is sRGB. For scanners, monitors, and
printers that (more or less) conform to sRGB, no ICC profile is
needed.

The implementation will be very well integrated with the Gimp, but we
would also like this library to be used by other applications that
could benefit from color matching. Consequently, it is being developed
under dual LGPL and MPL license. Our model here is the Independent
Jpeg Group's implementation of the JPEG standard, which is both
technically excellent and licensed on terms suitable for almost all
software, whether free or proprietary. We would like to make sure that
all enhancements remain licensed as free software, as well.

The mailing list for gcmm development is icc@gimp.org.

The original README written by ciccio is in README.ciccio

Other recommended documents:

http://www.color.org/ - the ICC homepage, including the ICC profile
specification

http://www.color.org/contrib/sRGB.html - the sRGB standard

http://www.ee.port.ac.uk/FAQs/colourspace-faq - the colorspace FAQ

http://www.fogra.org/ - FOGRA, the German graphic arts association. A
source of publicly available ICC profiles for offset printing.

http://www.pantone.com/

http://docs.sun.com:80/ab2/@Ab1CollToc?abcardcat=%2Fsafedir%2Fspace3%2Fpkgs%2Fcollections%2Fab1%2Fanswerbooks%2Fenglish%2Fsolaris_2.6%2FSUNWakcs%2Fab_cardcatalog;subject=tools
Sun's KCMS documentation

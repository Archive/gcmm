/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#ifndef __GCMMUTIL_H__
#define __GCMMUTIL_H__

#include <stdio.h>

#ifdef LITTLE_ENDIAN
extern icUInt16Number gcmmswap16 (icUInt16Number val);
extern icUInt32Number gcmmswap32 (icUInt32Number val);
extern icUInt64Number gcmmswap64 (icUInt64Number val);
#endif /* little endian */

extern char *gcmmstrbcd8 (icUInt8Number val);
extern icUInt8Number gcmmbcd8str (char *str);
extern char *gcmmstrbcd16 (icUInt16Number val);
extern icUInt16Number gcmmbcd16str (char *str);
extern char *gcmmstrbcd32 (icUInt32Number val);
extern icUInt32Number gcmmbcd32str (char *str);
extern double gcmmdu8f8 (icU8Fixed8Number val);
extern icU8Fixed8Number gcmmu8f8d (double val);
extern double gcmmds15f16 (icS15Fixed16Number val);
extern icS15Fixed16Number gcmms15f16d (double val);
extern double gcmmdu16f16 (icU16Fixed16Number val);
extern icU16Fixed16Number gcmmu16f16d (double val);
extern icUInt8Number gcmmLstar8encode (double val);
extern icUInt16Number gcmmLstar16encode (double val);
extern double gcmmLstar8decode (icUInt8Number val);
extern double gcmmLstar16decode (icUInt16Number val);
extern icUInt8Number gcmmencodeabstar8 (double val);
extern icUInt16Number gcmmencodeabstar16 (double val);
extern double gcmmdecodeabstar8 (icUInt8Number val);
extern double gcmmdecodeabstar16 (icUInt16Number val);
extern double gcmmastar2std (double val);
extern double gcmmastar2ic (double val);
extern double gcmmbstar2std (double val);
extern double gcmmbstar2ic (double val);
extern char *gcmmstricdate (icDateTimeNumber *date);
extern struct tm *gcmmtmicdate (icDateTimeNumber *date);
extern icDateTimeNumber *gcmmicdatetm (struct tm *tm);
extern int gcmmget8 (FILE *fp, icUInt8Number *val);
extern int gcmmget16 (FILE *fp, icUInt16Number *val);
extern int gcmmget32 (FILE *fp, icUInt32Number *val);
extern int gcmmget64 (FILE *fp, icUInt64Number *val);
extern int gcmmgetstr (FILE *fp, icUInt8Array *ptr, icUInt32Number size);
extern int gcmmgetsstr (FILE *fp, icUInt8Array ptr, icUInt32Number size);
extern int gcmmput8 (FILE *fp, icUInt8Number val);
extern int gcmmput16 (FILE *fp, icUInt16Number val);
extern int gcmmput32 (FILE *fp, icUInt32Number val);
extern int gcmmput64 (FILE *fp, icUInt64Number val);
extern int gcmmputstr (FILE *fp, icUInt8Array ptr, icUInt32Number size);
extern int gcmmisicc (char *name);
extern char *gcmmstrversion (icUInt32Number vers);
extern icUInt32Number gcmmversionstr (char *vers);
extern int gcmmncomponents (icColorSpaceSignature cs);
extern int gcmmfindtag (icProfile *profile, icSignature sig);

#endif

/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>

void cpmatrix (double dst[3][3], double src[3][3]);
void mulmatrix (double m[3][3], double f[3], double res[3]);
int invertmatrix (double matrix[3][3]);

typedef enum boolean
{
  false = 0, true
}BOOLEAN;

void
main (void)
{
  double out[3], test[3], in[3] = {77.0, 88.0, 99.0};
  double mi[3][3], m[3][3] =
  {
    {0.412453, 0.357580, 0.180423},
    {0.212671, 0.715160, 0.072169},
    {0.019334, 0.119193, 0.950227}
  };

  cpmatrix (mi, m);

  if (!invertmatrix (mi))
  {
    printf ("No solution\n");
    exit (1);
  }

  printf ("Inverted Matrix:\n");
  printf ("%f\t%f\t%f\n", mi[0][0], mi[0][1], mi[0][2]);
  printf ("%f\t%f\t%f\n", mi[1][0], mi[1][1], mi[1][2]);
  printf ("%f\t%f\t%f\n", mi[2][0], mi[2][1], mi[2][2]);

  mulmatrix (m, in, out);
  mulmatrix (mi, out, test);

  printf ("%f = %f\n", in[0], test[0]);
  printf ("%f = %f\n", in[1], test[1]);
  printf ("%f = %f\n", in[2], test[2]);
}

void
cpmatrix (double dst[3][3], double src[3][3])
{
  int i, j;

  for (j = 0; j < 3; j++)
    for (i = 0; i < 3; i++)
      dst[j][i] = src[j][i];
}

void
mulmatrix (double m[3][3], double f[3], double res[3])
{
  int i;

  for (i=0; i<3; i++)
    res[i] = (m[i][0] * f[0]) + (m[i][1] * f[1]) + (m[i][2] * f[2]);
}


int
invertmatrix (double matrix[3][3])
{
  double m[3][3], A = 0;
  int col, row;
  int p, q, a, b, s;

  cpmatrix (m, matrix);

  for (col = 0; col < 3; col++)
  {
    p = 1;
    q = 2;
    s = 1;

    if (col > 0)
    {
      p = 0;
      if (col == 2)
	q = 1;
    }

    if (col == 1)
      s = -1;

    A = A + s * m[0][col] * (m[1][p] * m[2][q] - m[2][p] * m[1][q]);
  }

  if (!A)
    return false;

  for (row = 0; row < 3; row++)
  {
    for (col = 0; col < 3; col++)
    {
      a = 1;
      b = 2;
      p = 1;
      q = 2;

      if (col > 0)
      {
	a = 0;
	if (col == 2)
	  b = 1;
      }
      if (row > 0)
      {
	p = 0;
	if (row == 2)
	  q = 1;
      }

      s = 1 - ((col + row) % 2) * 2;
      matrix[row][col] = s * (m[a][p] * m[b][q] - m[b][p] * m[a][q]) / A;
    }
  }
  return true;
}

/*------------------------------------------------------------------------*/
#if 0
/*
REM invert mat(s1,m1) into mat(s2,m2)

PROC invert(m1%,s1%,m2%,s2%)
A=0


FOR col%=0 TO 2
  p%=1:q%=2:s%=1
  IF col%>0
    p%=0
    IF col%=2
      q%=1
    ENDIF
  ENDIF
  IF col%=1
    s%=-1
  ENDIF
  A=A+s%*mat(s1%,m1%,0,col%)*(mat(s1%,m1%,1,p%)*mat(s1%,m1%,2,q%)-mat(s1%,m1
%,2,p%)*mat(s1%,m1%,1,q%))
NEXT



IF A=0
  PRINT "No solution, determinant is zero"
  RETURN
ENDIF
FOR row%=0 TO 2
  FOR col%=0 TO 2
    a%=1:b%=2:p%=1:q%=2
    IF col%>0
      a%=0
      IF col%=2
        b%=1
      ENDIF
    ENDIF
    IF row%>0
      p%=0
      IF row%=2
        q%=1
      ENDIF
    ENDIF
    s%=1-((row%+col%)MOD2)*2
    mat(s2%,m2%,row%,col%)=s%*(mat(s1%,m1%,a%,p%)*mat(s1%,m1%,b%,q%)-mat(s1%
,m1%,b%,p%)*mat(s1%,m1%,a%,q%))/A
  NEXT
NEXT
ENDPROC
*/
#endif
/* ---------------------------------------------------------*/


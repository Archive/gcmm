/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <math.h>

/*
 * B�zier:
 * Given four points of a B�zier-curve: p1, p2, p3, p4.
 * 1. find the control points:
 *       bezierctrl (p1, p2, p3, p4, &c1, &c2);      for each axis
 * 2. find the t-th point on curve:
 *       bezier (p1, c1, c2, p4, t);                 for each axis
 * where t goes from p1 (0.0) to p4 (1.0). If t == 1/3, the result is
 * p2, if t == 2/3, the result is p3.
 * SEE ALSO
 *      end of this file
 */

void bezierctrl (double p1, double p2, double p3, double p4, double *c1,
		 double *c2);
double bezier (double p1, double c1, double c2, double p4, double t);

void
main (void)
{
  double p1, p2, p3, p4;
  double c1, c2;
  double res;

  p1 = 37.37;
#if 1
  p2 = 14.14;
  p3 = 57.57;
#else
  p2 = 14.57;
  p3 = 14.57;
#endif
  p4 = 99.99;

  printf ("p1 = %f\np2 = %f\np3 = %f\np4 = %f\n", p1, p2, p3, p4);
  bezierctrl (p1, p2, p3, p4, &c1, &c2);
  printf ("c1 = %f\nc2 = %f\n", c1, c2);

  res = bezier (p1, c1, c2, p4, 0.0);
  printf ("res0 = %f\n", res);

  res = bezier (p1, c1, c2, p4, 1.0/3.0);
  printf ("res1/3 = %f\n", res);

  res = bezier (p1, c1, c2, p4, 2.0/3.0);
  printf ("res2/3 = %f\n", res);

  res = bezier (p1, c1, c2, p4, 1);
  printf ("res3 = %f\n", res);

  res = bezier (p1, c1, c2, p4, 0.5);
  printf ("res.5 = %f\n", res);
}

void
bezierctrl (double p1, double p2, double p3, double p4, double *c1, double *c2)
{
  *c1 = ((36.0 * p2) - (10.0 * p1) + (4.0 * p4) - (18.0 * p3)) / 12.0;
  *c2 = ((27.0 * p3) - p1 - (6.0 * (*c1)) - (8.0 * p4)) / 12.0;
}

double
bezier (double p1, double c1, double c2, double p4, double t)
{
  return (pow (1 - t, 3) * p1) + (3 * pow (1 - t, 2) * t * c1)
    + (3 * (1 - t) * pow (t, 2) * c2) + (pow (t, 3) * p4);
}

/*
 * interpolating a point in a bezier curve given as a table:
 * P:                     the interpolated point, the result.
 * P0:                    point we're looking for
 * P1, P2:                the lower and higher point for P0
 * p1, p2, p3, p4:        4 known points; two of them == P1, P2
 * c1, c1:                control points for p1..p4
 * C1, C2:                control points for P1..P2
 * 1. if the table has only 1 point, it makes no sense.
 *    if the table has only 2 points, make a linear interpolation
 *    if the table has only 3 points, set p2 == p3 (-> c1 == c2)
 * 2. choose a set of 4 points including P1 and P2 => p1, p2, p3, p4
 * 3. find the control point for this bezier segment => c1, c2
 * 4. find points t=1/3 and t=2/3 between P1 and P2 => P1', P2'
 * 5. find the control points for this new (smaller segment). We have
 *    now a bezier segment that starts and ends with the closest known
 *    points to the point we're looking for. => C1, C2
 * 6. compute a t for P within P1 and P2
 * 7. find P using P1, C1, C2, P2 and t
 */

/* The GIMP -- an image manipulation library
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *OR
 *
 *
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <math.h>
#include "gcmm.h"
#include "gcmmutil.h"
#include "gcmmvalue.h"

#define EPSILON 0.0001

typedef union Tableptr
{
  void *v;
  unsigned short *s;
  unsigned char *b;
}TABLEPTR;

static int do_lut (icProfile *profile, unsigned short *color, int to_pcs);
static int do_trc (icProfile *profile, unsigned short *color, int to_pcs);
int do_gamut (icProfile *profile, unsigned short *color);
int do_unlut (icProfile *profile, unsigned short *color);

/*
 * profile: pointer returned from gcmmread.
 * color:   RGB/CMY: scaled for 16 bits, else icc notation
 * to_pcs:  flag indicating to pcs or from pcs conversion.
 */

int
gcmmapply (icProfile *profile, unsigned short *color, int to_pcs)
{
  int has_lut;

  has_lut = gcmmgetlutinputchannels (profile, "perceptual", to_pcs);

  switch (profile->header.deviceClass)
  {
    case icSigInputClass:
      if (has_lut && do_lut (profile, color, to_pcs))
	return true;
      else if (do_trc (profile, color, to_pcs))
	return true;
      else
	return false;
    case icSigDisplayClass:
      if (has_lut && do_lut (profile, color, to_pcs))
	return true;
      else if (do_trc (profile, color, to_pcs))
	return true;
      else
	return false;
    case icSigOutputClass:
      if (has_lut && do_lut (profile, color, to_pcs))
	return true;
      else if (do_trc (profile, color, to_pcs))
	return true;
      else
	return false;
    case icSigLinkClass:
      printf ("Don't know how to handle link profiles\n");
      break;
    case icSigAbstractClass:
      printf ("Don't know how to handle abstract profiles\n");
      break;
    case icSigColorSpaceClass:
      printf ("Don't know how to handle conversion profiles\n");
      break;
    case icSigNamedColorClass:
      printf ("Don't know how to handle named color profiles\n");
      break;
    default:
      printf ("Don't know how to handle this profiles\n");
      return false;
  }
  return true;
}

static int
do_lut (icProfile *profile, unsigned short *color, int to_pcs)
{
  TABLEPTR ptr, ptr1;
  int ichannels, ochannels, i, count;
  unsigned short res[MAX_CHAN];
  double val[MAX_CHAN], in[MAX_CHAN], fr[MAX_CHAN];
  double unit, dist, error;
  int lo[MAX_CHAN], hi[MAX_CHAN], LO[MAX_CHAN], HI[MAX_CHAN];
  int luttype, highval;

  luttype = gcmmgetluttype (profile, "perceptual", to_pcs);
  ichannels = gcmmgetlutinputchannels (profile, "perceptual", to_pcs);
  highval = to_pcs ? profile->highval : 65535;      /* XYZ/Lab is always 16b */

  if (!to_pcs && profile->header.pcs == icSigXYZData)
    printf ("Here I should apply a matrix, but I don't :-)\n");

  count = gcmmgetlutinputentries (profile, "preceptual", to_pcs);

  dist = error = 0.0;
  for (i=0; i<ichannels; i++)
  {
    if (highval != count - 1)
    {
      val[i] = (double)(color[i] * (count - 1)) / highval;
      fr[i] = modf (val[i], &(in[i]));
      dist += fr[i] * fr[i];
    }
    else
    {
      fr[i] = 0.0;
      in[i] = val[i] = color[i];
    }
    lo[i] = in[i];
    hi[i] = (fr[i] > EPSILON) ? lo[i] + 1 : lo[i];

    if ((ptr.v = gcmmgetlutinput (profile, "perceptual", to_pcs, i)) == NULL)
      return false;
    HI[i] = (luttype == 256) ? ptr.b[hi[i]] : ptr.s[hi[i]];
    res[i] = LO[i] = (luttype == 256) ? ptr.b[lo[i]] : ptr.s[lo[i]];
  }
  unit = sqrt (ichannels);
  dist = sqrt (dist);
  error = dist / unit;

  /*
   * This is no color space conversion yet. Input and output pertain to
   * the same color space; so the error isn't unified but appied on a
   * channel base.
   */
  for (i=0; i<ichannels; i++)
    if (hi[i] != lo[i])
      res[i] = (LO[i] + ((HI[i] - LO[i]) * fr[i])) + 0.5;

  /*
   * These values again should be between 0..65535 | 0..256.
   * Now we have to scale it down to the number of clutpoints:
   */

  count = gcmmgetlutgridpoints (profile, "perceptual", to_pcs);

  dist = error = 0.0;
  for (i=0; i<ichannels; i++)
  {
    if (luttype != count)
    {
      val[i] = (double)(res[i] * (count - 1)) / (luttype - 1);
      fr[i] = modf (val[i], &(in[i]));
      dist += fr[i] * fr[i];
    }
    else
    {
      fr[i] = 0.0;
      in[i] = val[i] = res[i];
    }
    lo[i] = in[i];
    hi[i] = (fr[i] > EPSILON) ? lo[i] + 1 : lo[i];
  }
  dist = sqrt (dist);
  error = dist / unit;

  ptr1.v = NULL;
  switch (ichannels)
  {
    case 3:
      ptr.v = gcmmgetlutentry (profile, "perceptual", to_pcs,
			     lo[0], lo[1], lo[2]);
      if (error > EPSILON)
	ptr1.v = gcmmgetlutentry (profile, "perceptual", to_pcs,
				hi[0], hi[1], hi[2]);
      break;
    case 4:
      ptr.v = gcmmgetlutentry (profile, "perceptual", to_pcs,
			     lo[0], lo[1], lo[2], lo[3]);
      if (error > EPSILON)
	ptr1.v = gcmmgetlutentry (profile, "perceptual", to_pcs,
				hi[0], hi[1], hi[2], hi[3]);
      break;
  }
  if (!ptr.v)
    return false;

  ochannels = gcmmgetlutoutputchannels (profile, "perceptual", to_pcs);
  for (i = 0; i < ochannels; i++)
  {
    res[i] = LO[i] = HI[i] = (luttype == 256) ? ptr.b[i] : ptr.s[i];
    if (ptr1.v)
      HI[i] = (luttype == 256) ? ptr1.b[i] : ptr1.s[i];
  }

  for (i = 0; i < ochannels; i++)
    if (hi[i] != lo[i])
      res[i] = (LO[i] + ((HI[i] - LO[i]) * error)) + 0.5;


  /*
   * Now apply output table
   */

  count = gcmmgetlutoutputentries (profile, "preceptual", to_pcs);

  dist = error = 0.0;
  for (i = 0; i < ochannels; i++)
  {
    if (luttype != count)
    {
      val[i] = (double)(res[i] * (count - 1)) / (luttype - 1);
      fr[i] = modf (val[i], &(in[i]));
      dist += fr[i] * fr[i];
    }
    else
    {
      fr[i] = 0.0;
      in[i] = val[i] = res[i];
    }
    lo[i] = in[i];
    hi[i] = (fr[i] > EPSILON) ? lo[i] + 1 : lo[i];

    if ((ptr.v = gcmmgetlutoutput (profile, "preceptual", to_pcs, i)) == NULL)
      return false;

    HI[i] = (luttype == 256) ? ptr.b[hi[i]] : ptr.s[hi[i]];
    res[i] = LO[i] = (luttype == 256) ? ptr.b[lo[i]] : ptr.s[lo[i]];
  }
  unit = sqrt (ochannels);
  dist = sqrt (dist);
  error = dist / unit;

  /*
   * as in the case of the input table, this is not a color space
   * conversion (which has already been performed), but some kind
   * of (un)linearization. So the error is interpolated on a channel
   * base.
   */

  for (i = 0; i < ochannels; i++)
    if (hi[i] != lo[i])
      res[i] = (LO[i] + ((HI[i] - LO[i]) * fr[i])) + 0.5;

  for (i = 0; i < ochannels; i++)
    color[i] = res[i];

  return true;
}

int
do_gamut (icProfile *profile, unsigned short *color)
{
  TABLEPTR ptr;
  int ichannels, i, count;
  unsigned short res[MAX_CHAN];
  int luttype, highval;

  luttype = gcmmgetluttype (profile, "gamut", 0);
  ichannels = gcmmgetlutinputchannels (profile, "gamut", 0);
  highval = 65535;      /* XYZ/Lab is always 16b */

  if (profile->header.pcs == icSigXYZData)
    printf ("Here I should apply a matrix, but I don't :-)\n");

  count = gcmmgetlutinputentries (profile, "gamut", 0);

  for (i=0; i<ichannels; i++)
  {
    res[i] = (int)((double)(color[i] * (count - 1)) / (double)highval + 0.5);

    if ((ptr.v = gcmmgetlutinput (profile, "gamut", 0, i)) == NULL)
      return false;
    res[i] = (luttype == 256) ? ptr.b[res[i]] : ptr.s[res[i]];
  }

  count = gcmmgetlutgridpoints (profile, "gamut", 0);

  for (i=0; i<ichannels; i++)
    res[i] = (int)((double)(res[i] * (count-1)) / (double)(luttype-1) + 0.5);

  ptr.v = gcmmgetlutentry (profile, "gamut", 0, res[0], res[1], res[2]);
  if (!ptr.v)
    return false;

  res[0] = (luttype == 256) ? ptr.b[0] : ptr.s[0];

  count = gcmmgetlutoutputentries (profile, "gamut", 0);

  res[0] = (int)((double)(res[0] * (count-1)) / (double)(luttype-1) + 0.5);
  if ((ptr.v = gcmmgetlutoutput (profile, "gamut", 0, 0)) == NULL)
    return false;
  res[0] = (luttype == 256) ? ptr.b[res[0]] : ptr.s[res[0]];

  return res[0];
}

int
do_unlut (icProfile *profile, unsigned short *color)
{
  int ichannels, ochannels, i, count;
  unsigned short *ptr = NULL;
  unsigned short res[MAX_CHAN];
  double val[MAX_CHAN], in[MAX_CHAN], fr[MAX_CHAN], a;
  double unit, UNIT, dist, DIST, error;
  int lo[MAX_CHAN], hi[MAX_CHAN], LO[MAX_CHAN], HI[MAX_CHAN];

  /*
   * if PCS == XYZ  we have to apply the matrix
   */

  /*
   * First apply input table
   */

  ichannels = gcmmgetlutinputchannels (profile, "perceptual", false);
  count = gcmmgetlutinputentries (profile, "preceptual", false);

  unit = UNIT = dist = DIST = error = 0.0;
  for (i=0; i<ichannels; i++)
  {
    val[i] = color[i] * (count - 1);
    val[i] /= 65536.0;
    fr[i] = modf (val[i], &(in[i]));
    hi[i] = lo[i] = in[i];
    if (fr[i] > EPSILON)
    {
      hi[i]++;
      dist += 1;
      error += fr[i] * fr[i];
    }
    ptr = gcmmgetlutinput (profile, "preceptual", false, i);
    LO[i] = ptr[lo[i]];
    HI[i] = ptr[hi[i]];
    a = HI[i] - LO[i];
    UNIT += a * a;
  }
  dist = pow (dist, 1.0 / ichannels);
  error = pow (error, 1.0 / ichannels) / dist;
  DIST = pow (UNIT, 1.0 / ichannels) * error;

  for (i=0; i<ichannels; i++)
    if (hi[i] == lo[i])
      res[i] = lo[i];
    else
      res[i] = (LO[i] + ((HI[i] - LO[i]) * error)) + 0.5;

  /*
   * These values are between 0..65535 or 0..255 (lut16/lut8).
   * Now we have to scale it down to the number of clutpoints:
   */

  count = gcmmgetlutgridpoints (profile, "perceptual", false);

  unit = UNIT = dist = DIST = error = 0.0;
  for (i=0; i<ichannels; i++)
  {
    val[i] = res[i] * (count - 1);
    val[i] /= gcmmgetluttype (profile, "perceptual", false) - 1;
    fr[i] = modf (val[i], &(in[i]));
    hi[i] = lo[i] = in[i];
    if (fr[i] > EPSILON)
    {
      hi[i]++;
      dist += 1;
      error += fr[i] * fr[i];
    }
  }

  ochannels = gcmmgetlutoutputchannels (profile, "perceptual", false);
  if (hi[i] == lo[i])
  {
    switch (ochannels)
    {
      case 3:
	ptr = gcmmgetlutentry (profile, "perceptual", false,
			     res[0], res[1], res[2]);
	break;
      case 4:
	ptr = gcmmgetlutentry (profile, "perceptual", false,
			     res[0], res[1], res[2], res[3]);
	break;
    }
    for (i=0; i<ochannels; i++)
      res[i] = ptr[i];
  }
  else
  {
    switch (ochannels)
    {
      case 3:
	ptr = gcmmgetlutentry (profile, "perceptual", false,
			     lo[0], lo[1], lo[2]);
	for (i=0; i<ochannels; i++)
	  LO[i] = ptr[i];
	ptr = gcmmgetlutentry (profile, "perceptual", false,
			     hi[0], hi[1], hi[2]);
	for (i=0; i<ochannels; i++)
	  HI[i] = ptr[i];
	break;
      case 4:
	ptr = gcmmgetlutentry (profile, "perceptual", false,
			     lo[0], lo[1], lo[2], lo[3]);
	for (i=0; i<ochannels; i++)
	  LO[i] = ptr[i];
	ptr = gcmmgetlutentry (profile, "perceptual", false,
			     hi[0], hi[1], hi[2], hi[3]);
	for (i=0; i<ochannels; i++)
	  HI[i] = ptr[i];
	break;
    }

    for (i=0; i<ochannels; i++)
    {
      a = HI[i] - LO[i];
      UNIT += a * a;
    }
    dist = pow (dist, 1.0 / ichannels);
    error = pow (error, 1.0 / ichannels) / dist;
    DIST = pow (UNIT, 1.0 / ochannels) * error;
    for (i=0; i<ochannels; i++)
      res[i] = (LO[i] + ((HI[i] - LO[i]) * error)) + 0.5;
  }

  /*
   * Now apply output table
   */

  count = gcmmgetlutoutputentries (profile, "preceptual", true);

  unit = UNIT = dist = DIST = error = 0.0;
  for (i=0; i<ochannels; i++)
  {
    val[i] = res[i] * count;
    val[i] /= gcmmgetluttype (profile, "perceptual", true);
    fr[i] = modf (val[i], &(in[i]));
    hi[i] = lo[i] = in[i];
    if (fr[i] > EPSILON)
    {
      hi[i]++;
      dist += 1;
      error += fr[i] * fr[i];
    }
    ptr = gcmmgetlutoutput (profile, "preceptual", true, i);
    LO[i] = ptr[lo[i]];
    HI[i] = ptr[hi[i]];

    if (hi[i] != lo[i])
    {
      a = HI[i] - LO[i];
      UNIT += a * a;
    }
  }
  dist = pow (dist, 1.0 / ochannels);
  error = pow (error, 1.0 / ochannels) / dist;
  DIST = pow (UNIT, 1.0 / ochannels) * error;

  for (i=0; i<ochannels; i++)
    if (hi[i] == lo[i])
      res[i] = LO[i];
    else
      res[i] = (LO[i] + ((HI[i] - LO[i]) * error)) + 0.5;

  if (gcmmgetluttype (profile, "perceptual", true) == 256)
  {
    for (i=0; i<ochannels; i++)
      color[i] = res[i] * 256;
  }
  else
  {
    for (i=0; i<ochannels; i++)
      color[i] = res[i];
  }

  return true;
}

static int
do_trc (icProfile *profile, unsigned short *color, int to_pcs)
{
  unsigned short res[3];
  double matrix[9], *ptr;
  int i;

printf ("This is untested and will most probably break.\n");

  for (i=0; i<3; i++)
  {
    int count = gcmmgetTRCcount (profile, i);
    double val;

    val = color[i] * count;
    val /= 65536.0;
    res[i] = gcmmgetTRC (profile, i, (int)(val + 0.5));
    ptr = gcmmgetcolorant (profile, i);
    matrix [i * 3] = ptr[0];
    matrix [i * 3 + 1] = ptr[1];
    matrix [i * 3 + 2] = ptr[2];
  }

  /*
   * res: unsigned short; matrix: double; maybe I should be more careful
   * with type conversions...
   */

  res[0] = res[0] * matrix[0] + res[1] * matrix[1] + res[2] * matrix[2];
  res[1] = res[0] * matrix[3] + res[1] * matrix[4] + res[2] * matrix[5];
  res[2] = res[0] * matrix[6] + res[1] * matrix[7] + res[2] * matrix[8];

  color[0] = res[0];
  color[1] = res[1];
  color[2] = res[2];

  return true;
}

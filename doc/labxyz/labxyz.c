/* The GIMP -- an image manipulation library
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *OR
 *
 *
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <math.h>

void
main (void)
{
  double X, Y, Z;
  double Ls, as, bs;
  double wX, wY, wZ;
  double XXn, YYn, ZZn;
  double fX, fY, fZ;

#if 0
  /*
   * Any
   * RGB = (32 128 255) XYZ100 = (41.167295 45.783940 101.248344)
   */

  X = 41.167295;
  Y = 45.783940;
  Z = 101.248344;
#endif

#if 0
  /*
   * black
   * RGB = (0 0 0) XYZ = (0.000000 0.000000 0.000000)
   */
  X = 0.0;
  Y = 0.0;
  Z = 0.0;
#endif

#if 0
  /*
   * white
   * RGB = (255 255 255) XYZ = (95.045600 100.000000 108.875400)
   */

  X = 95.0456;
  Y = 100.0;
  Z = 108.8754;
#endif

#if 0
  /*
   * Red
   * RGB = (255 0 0) XYZ = (41.245300 21.267100 1.933400)
   */
  X = 41.245300;
  Y = 21.267100;
  Z = 1.933400;
#endif

#if 0
  /*
   * Green
   * RGB = (0 255 0) XYZ = (35.758000 71.516000 11.919300)
   */
  X = 35.758000;
  Y = 71.516000;
  Z = 11.919300;
#endif

#if 1
  /*
   * Blue
   * RGB = (0 0 255) XYZ = (18.042300 7.216900 95.022700)
   */

  X = 18.042300;
  Y = 7.216900;
  Z = 95.022700;
#endif

  wX = 95.0456;
  wY = 100.0;
  wZ = 108.8754;

  XXn = X / wX;
  YYn = Y / wY;
  ZZn = Z / wZ;

  if (YYn > 0.008856)
    Ls = (116 * pow (YYn, 1.0/3.0)) - 16;
  else
    Ls = 903.3 * YYn;

  if (XXn > 0.008856)
    fX = pow (XXn, 1.0/3.0);
  else
    fX = 7.787 * XXn + (16 / 116);

  if (YYn > 0.008856)
    fY = pow (YYn, 1.0/3.0);
  else
    fY = 7.787 * YYn + (16 / 116);

  if (ZZn > 0.008856)
    fZ = pow (ZZn, 1.0/3.0);
  else
    fZ = 7.787 * ZZn + (16 / 116);

  as = 500 * (fX - fY);
  bs = 200 * (fY - fZ);

  printf ("Lab (%f %f %f)\n", Ls, as, bs);
}

/* The GIMP -- an image manipulation library
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *OR
 *
 *
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
 * three arguments in range 0..100
 */

void
main (int argc, char **argv)
{
  double X, Y, Z;
  double Ls, as, bs;
  double wX, wY, wZ;
  double fY;

  if (argc < 4)
    exit (1);
  Ls = strtod (argv[1], NULL);
  as = strtod (argv[2], NULL);
  bs = strtod (argv[3], NULL);

  wX = 95.0456;
  wY = 100.0;
  wZ = 108.8754;

  if (Ls <= 8)
  {
    Y = (Ls * wY) / 903.3;
    fY = (7.787 * (Y / wY)) + (16.0 / 116.0);
  }
  else
  {
    Y = wY * pow ((Ls + 16.0) / 116.0, 3.0);
    fY = pow (Y / wY, 1.0 / 3.0);
  }

  X = wX * pow ((as / 500.0) + fY, 3.0);
  if (X / wX <= 0.008856)
    X = (wX * ((as / 500.0) + fY - (16.0 / 116.0))) / 7.787;

  Z = wZ * pow (fY - (bs / 200.0), 3.0);
  if (Z / wZ <= 0.008859)
    Z = (wZ * (fY - (bs / 200.0) - (16.0 / 116.0))) / 7.787;

  printf ("XYZ (%.8f %.8f %.8f) \n", X, Y, Z);
}

/* The GIMP -- an image manipulation library
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *OR
 *
 *
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>

double m1[3][3] =         /* XYZ <- RGB */
{
  {0.412453, 0.357580, 0.180423},
  {0.212671, 0.715160, 0.072169},
  {0.019334, 0.119193, 0.950227}
};

double m2[3][3] =         /* RGB <- XYZ */
{
  {3.240479, -1.537150, -0.498535},
  {-0.969256, 1.875992, 0.041556},
  {0.055648, -0.204043, 1.057311}
};

void
scale100 (double *R, double *G, double *B)
{
  *R = *R * 100 / 255;
  *G = *G * 100 / 255;
  *B = *B * 100 / 255;
}

void
scale000 (double *R, double *G, double *B)
{
  *R = *R / 255;
  *G = *G / 255;
  *B = *B / 255;
}

void
main (void)
{
  double X, Y, Z;
  double R, G, B;
  double r, g, b;
  double step = 32;

  for (R = 0.0; R < 257.0 ; R += step)
    for (G = 0.0; G < 257.0 ; G += step)
      for (B = 0.0; B < 257.0 ; B += step)
      {
	if (R == 256.0)
	  R = 255;
	if (G == 256.0)
	  G = 255;
	if (B == 256.0)
	  B = 255;

	r = R;
	g = G;
	b = B;
#if 1
	scale000 (&r, &g, &b);
#else
	scale100 (&r, &g, &b);
#endif

	X = m1[0][0] * r + m1[0][1] * g + m1[0][2] * b;
	Y = m1[1][0] * r + m1[1][1] * g + m1[1][2] * b;
	Z = m1[2][0] * r + m1[2][1] * g + m1[2][2] * b;

	printf ("RGB = (%3.0f %3.0f %3.0f)\t", R, G, B);
	printf ("XYZ = (%f %f %f)\n", X, Y, Z);
      }
}

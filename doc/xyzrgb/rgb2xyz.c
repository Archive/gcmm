/* The GIMP -- an image manipulation library
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *OR
 *
 *
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>

double m1[3][3] =         /* XYZ <- RGB */
{
  {0.412453, 0.357580, 0.180423},
  {0.212671, 0.715160, 0.072169},
  {0.019334, 0.119193, 0.950227}
};

double m2[3][3] =         /* RGB <- XYZ */
{
  {3.240479, -1.537150, -0.498535},
  {-0.969256, 1.875992, 0.041556},
  {0.055648, -0.204043, 1.057311}
};

void
main (int argc, char **argv)
{
  double R, G, B;
  double X, Y, Z;
  double x, y, z;

  if (argc < 4)
    exit (1);
  R = strtod (argv[1], NULL);
  G = strtod (argv[2], NULL);
  B = strtod (argv[3], NULL);

  X = m1[0][0] * R + m1[0][1] * G + m1[0][2] * B;
  Y = m1[1][0] * R + m1[1][1] * G + m1[1][2] * B;
  Z = m1[2][0] * R + m1[2][1] * G + m1[2][2] * B;

  X = X * 100 / 255;
  Y = Y * 100 / 255;
  Z = Z * 100 / 255;

  x = X / (X + Y + Z);
  y = Y / (X + Y + Z);
  z = 1 - x - y;

  printf ("XYZ = (%f %f %f) ", X, Y, Z);
  printf ("Yxy = (%f %f %f)", Y, y, z);
  printf ("\n");
}

/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void
main (int argc, char **argv)
{
  FILE *fp;
  double color[3], col[3], dist, DIF = 9999999.9, cdist, closest[3];
  char buf[BUFSIZ], *ptr;

  if (argc < 4)
    exit (1);

  color[0] = strtod (argv[1], NULL);
  color[1] = strtod (argv[2], NULL);
  color[2] = strtod (argv[3], NULL);

  dist = color[0] * color[0];
  dist += color[1] * color[1];
  dist += color[2] * color[2];
  dist = pow (dist, 1.0 / 3.0);

  fp = fopen ("multi.tab", "r");
  if (!fp)
    exit (1);

  while (fgets (buf, BUFSIZ, fp))
  {
    col[0] = strtod (buf, &ptr);
    col[1] = strtod (ptr, &ptr);
    col[2] = strtod (ptr, &ptr);

    cdist = pow (col[0] - color[0], 2.0);
    cdist += pow (col[1] - color[1], 2.0);
    cdist += pow (col[2] - color[2], 2.0);
    cdist = pow (cdist, 1.0 / 3.0);

    if (fabs (cdist) < DIF)
    {
      DIF = fabs (cdist);
      closest[0] = col[0];
      closest[1] = col[1];
      closest[2] = col[2];
    }
  }
  fclose (fp);

  printf ("%0.0f %0.0f %0.0f\n", closest[0], closest[1], closest[2]);
}

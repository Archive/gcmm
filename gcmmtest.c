/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <math.h>
#include "gcmm.h"
#include "gcmmread.h"
#include "gcmmapply.h"
#include "gcmmutil.h"
#include "gcmmvalue.h"
#include "pngfuncs.h"

void
xyz2lab (double wX, double wY, double wZ, double X, double Y, double Z,
	 double *Ls, double *as, double *bs)
{
  double XXn, YYn, ZZn;
  double fX, fY, fZ;

  XXn = X / wX;
  YYn = Y / wY;
  ZZn = Z / wZ;

  if (YYn > 0.008856)
    *Ls = (116 * pow (YYn, 1.0/3.0)) - 16;
  else
    *Ls = 903.3 * YYn;

  if (XXn > 0.008856)
    fX = pow (XXn, 1.0/3.0);
  else
    fX = 7.787 * XXn + (16 / 116);

  if (YYn > 0.008856)
    fY = pow (YYn, 1.0/3.0);
  else
    fY = 7.787 * YYn + (16 / 116);

  if (ZZn > 0.008856)
    fZ = pow (ZZn, 1.0/3.0);
  else
    fZ = 7.787 * ZZn + (16 / 116);

  *as = 500 * (fX - fY);
  *bs = 200 * (fY - fZ);
}


unsigned short *
iccxyz2lab (icProfile *profile, unsigned short *color)
{
  double X, Y, Z;
  double Lstar, astar, bstar;
  double *white;

  white = gcmmgetwhitepoint (profile);
  white[0] *= 100;
  white[1] *= 100;
  white[2] *= 100;

  X = Y = Z = 0.0;
  if (color[0] & 0x8000)
  {
    X = 1.0;
    color[0] &= 0x7FFF;
  }
  X += (double)color[0] / 32768.0;

  if (color[1] & 0x8000)
  {
    Y = 1.0;
    color[1] &= 0x7FFF;
  }
  Y += (double)color[1] / 32768.0;

  if (color[2] & 0x8000)
  {
    Z = 1.0;
    color[2] &= 0x7FFF;
  }
  Z += (double)color[2] / 32768.0;

  X *= 100.0;
  Y *= 100.0;
  Z *= 100.0;

  xyz2lab (white[0], white[1], white[2], X, Y, Z, &Lstar, &astar, &bstar);

  color[0] = gcmmLstar16encode (Lstar);
  color[1] = gcmmencodeabstar16 (gcmmastar2ic (astar));
  color[2] = gcmmencodeabstar16 (gcmmbstar2ic (bstar));

  return color;
}

double
iccxyz2dbl (unsigned short val)
{
  double res = 0.0;

  if (val & 0x8000)
  {
    res = 1.0;
    val &= 0x7FFF;
  }

  res += (double)val / 32768.0;

  return res;
}

void
main (int argc, char **argv)
{
  icProfile *profile, *profile1;
  unsigned short color[MAX_CHAN];
  unsigned char **rows, *ptr;
  char *fname;
  int ht, wd;
  int h, w;

  if (argc < 3)
  {
    printf("Usage: %s input_profile output_profile [in-file.png] [out-file.png]\n", argv[0]);
    exit (1);
  }

  profile = gcmmread (argv[1]);
  if (!profile)
  {
    printf("error reading profile %s\n", argv[1]);
    exit (1);
  }

  profile1 = gcmmread (argv[2]);
  if (!profile1)
  {
    printf("error reading profile %s\n", argv[2]);
    exit (1);
  }

  if (argc < 4)
    fname = "in-file.png";
  else
    fname = argv[3];

  printf("reading image from %s\n", fname);
  if (!getpng (fname, &rows, &ht, &wd))
  {
    printf("error reading png file %s\n", fname);
    exit (1);
  }

  for (h=0; h<ht; h++)
  {
    ptr = rows[h];
    for (w=0; w<wd; w++)
    {
      color[0] = ptr[0];
      color[1] = ptr[1];
      color[2] = ptr[2];

      if (!gcmmapply (profile, color, 1))
	exit (1);

      iccxyz2lab (profile, color);
#if 0
      color[15] = do_gamut (profile1, color);
      if (color[15])
	color[0] = color[1] = color[2] = color[3] = color[15];
      else
#endif
	if (!gcmmapply (profile1, color, 0))
	  exit (1);

      ptr[0] = color[0];
      ptr[1] = color[1];
      ptr[2] = color[2];

      ptr += 3;
    }
  }
  if (argc < 5)
    fname = "out-file.png";
  else
    fname = argv[4];
  printf("writing image to %s\n", fname);
  if (!putpng (fname, rows, ht, wd))
  {
    printf("error writing png file %s\n", fname);
    exit (1);
  }
  exit (0);
}

/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "gcmm.h"
#include "gcmmutil.h"
#include "gcmmsignature.h"

icProfile *gcmmread (char *filename);
static int getheader (FILE *fp, icProfile *icProfile);
static int gettable (FILE *fp, icProfile *icProfile);
static void *mkmem (size_t size);
static int getCrdInfo (FILE *fp, icProfile *profile, icTagBase *base,
		       icTag *tag);
static int getCurve (FILE *fp, icProfile *profile, icTagBase *base,
		     icTag *tag);
static int getData (FILE *fp, icProfile *profile, icTagBase *base,
		    icTag *tag);
static int getDateTime (FILE *fp, icProfile *profile, icTagBase *base,
			icTag *tag);
static int getLut16 (FILE *fp, icProfile *profile, icTagBase *base,
		     icTag *tag);
static int getLut8 (FILE *fp, icProfile *profile, icTagBase *base,
		    icTag *tag);
static int getMeasurement (FILE *fp, icProfile *profile, icTagBase *base,
			   icTag *tag);
static int getNamedColor2 (FILE *fp, icProfile *profile, icTagBase *base,
			   icTag *tag);
static int getProfileSequenceDesc (FILE *fp, icProfile *profile,
				   icTagBase *base, icTag *tag);
static int getS15Fixed16Array (FILE *fp, icProfile *profile, icTagBase *base,
			  icTag *tag);
static int getScreening (FILE *fp, icProfile *profile, icTagBase *base,
			 icTag *tag);
static int getSignature (FILE *fp, icProfile *profile, icTagBase *base,
			 icTag *tag);
static int getTextDescription (FILE *fp, icProfile *profile, icTagBase *base,
			       icTag *tag);
static int getText (FILE *fp, icProfile *profile, icTagBase *base,
		    icTag *tag);
static int getU16Fixed16Array (FILE *fp, icProfile *profile, icTagBase *base,
			       icTag *tag);
static int getUcrBg (FILE *fp, icProfile *profile, icTagBase *base,
		     icTag *tag);
static int getUInt16Array (FILE *fp, icProfile *profile, icTagBase *base,
			   icTag *tag);
static int getUInt32Array (FILE *fp, icProfile *profile, icTagBase *base,
			   icTag *tag);
static int getUInt64Array (FILE *fp, icProfile *profile, icTagBase *base,
			   icTag *tag);
static int getUInt8Array (FILE *fp, icProfile *profile, icTagBase *base,
			  icTag *tag);
static int getViewingCondition (FILE *fp, icProfile *profile, icTagBase *base,
				icTag *tag);
static int getXYZ (FILE *fp, icProfile *profile, icTagBase *base,
		   icTag *tag);
static int getNamedColor (FILE *fp, icProfile *profile, icTagBase *base,
			  icTag *tag);

icProfile *
gcmmread (char *filename)
{
  icProfile *profile = NULL;
  FILE *fp;
  int i;

  if (!gcmmisicc (filename))
    return false;

  fp = fopen (filename, "r");
  if (!fp)
    return NULL;

  if ((profile = (icProfile *)mkmem (sizeof (icProfile))) == NULL)
    return NULL;

  if (!getheader (fp, profile))
    return NULL;

  if (!gettable (fp, profile))
    return NULL;

  /*---------------------------------------------------------------------
   * TABLE OF REQUIRED TAGS
   *
   * A1 = input monochrome
   * A2 = input 3-component
   * A3 = input LUT
   * B4 = display monochrome
   * B5 = display RGB
   * C6 = output monochrome
   * C7 = output RGB/CMYK
   * D8 = device link
   * D9 = color space conversion
   * DA = abstract
   * DB = name colors
   *
   *                    input  dis  out  l c a n
   *                    ------------------------
   *                    A A A  B B  C C  D D D D
   *                    1 2 3  4 5  6 7  8 9 A B
   *                    ------------------------
   * profileDescription x x x  x x  x x  x x x x
   * grayTRC            x - -  x -  x -  - - - -
   * mediawhite         x x x  x x  x x  - x x x
   * copyright          x x x  x x  x x  x x x x
   * rgb-colorant       - x -  - x  - -  - - - -
   * rgb-TRC            - x -  - x  - -  - - - -
   * AToB0              - - x  - -  - x  x x x -
   * AToB1              - - -  - -  - x  - - - -
   * AToB2              - - -  - -  - x  - - - -
   * BToA0              - - -  - -  - x  - x - -
   * BToA1              - - -  - -  - x  - - - -
   * BToA2              - - -  - -  - x  - - - -
   * gamut              - - -  - -  - x  - - - -
   * profile sequence   - - -  - -  - -  x - - -
   * named color2       - - -  - -  - -  - - - x
   *---------------------------------------------------------------------*/

  if (gcmmfindtag (profile, icSigProfileDescriptionTag) == -1)
    return NULL;

  if (gcmmfindtag (profile, icSigCopyrightTag) == -1)
    return NULL;

  if (profile->header.deviceClass != icSigLinkClass
      && gcmmfindtag (profile, icSigMediaWhitePointTag) == -1)
    return NULL;

  if (profile->header.deviceClass == icSigInputClass
      || profile->header.deviceClass == icSigDisplayClass)
  {
    if ((gcmmncomponents (profile->header.colorSpace) == 1)
	&& gcmmfindtag (profile, icSigGrayTRCTag) == -1)
      return NULL;
    else if (profile->header.pcs == icSigLabData
	     && gcmmfindtag (profile, icSigAToB0Tag) == -1)
      return NULL;
    else if (gcmmfindtag (profile, icSigAToB0Tag) == -1)
    {
      if (gcmmfindtag (profile, icSigRedColorantTag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigGreenColorantTag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigBlueColorantTag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigRedTRCTag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigGreenTRCTag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigBlueTRCTag) == -1)
	return NULL;
    }
  }

  if (profile->header.deviceClass == icSigOutputClass)
  {
    if (gcmmncomponents (profile->header.colorSpace) == 1
	&& gcmmfindtag (profile, icSigGrayTRCTag) == -1)
      return NULL;
    else
    {
      if (gcmmfindtag (profile, icSigAToB0Tag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigAToB1Tag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigAToB2Tag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigBToA0Tag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigBToA1Tag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigBToA0Tag) == -1)
	return NULL;
      if (gcmmfindtag (profile, icSigGamutTag) == -1)
	return NULL;
    }
  }

  if ((profile->header.deviceClass == icSigLinkClass
       || profile->header.deviceClass == icSigAbstractClass
       || profile->header.deviceClass == icSigColorSpaceClass)
      && gcmmfindtag (profile, icSigAToB0Tag) == -1)
    return NULL;

  if (profile->header.deviceClass == icSigLinkClass
      && gcmmfindtag (profile, icSigProfileSequenceDescTag) == -1)
    return NULL;

  if (profile->header.deviceClass == icSigColorSpaceClass
      && gcmmfindtag (profile, icSigBToA0Tag) == -1)
    return NULL;

  if (profile->header.deviceClass == icSigNamedColorClass
      && gcmmfindtag (profile, icSigNamedColor2Tag) == -1)
    return NULL;

  for (i = 0; i < profile->tags.count; i++)
  {
    icTagBase base;

    fseek (fp, profile->tags.tag[i].offset, SEEK_SET);
    if (!gcmmget32 (fp, &(base.sig)))
      return NULL;

    if (!gcmmget32 (fp, &(base.reserved)))
      return NULL;

    switch (base.sig)
    {
      case icSigCurveType:
	if (!getCurve (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigDataType:
	if (!getData (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigDateTimeType:
	if (!getDateTime (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigLut16Type:
	if (!getLut16 (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigLut8Type:
	if (!getLut8 (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigMeasurementType:
	if (!getMeasurement (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigNamedColorType:
	if (!getNamedColor (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigProfileSequenceDescType:
	if (!getProfileSequenceDesc (fp, profile, &base,
				     &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigS15Fixed16ArrayType:
	if (!getS15Fixed16Array (fp, profile, &base,
				 &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigScreeningType:
	if (!getScreening (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigSignatureType:
	if (!getSignature (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigTextType:
	if (!getText (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigTextDescriptionType:
	if (!getTextDescription (fp, profile, &base,
				 &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigU16Fixed16ArrayType:
	if (!getU16Fixed16Array (fp, profile, &base,
				 &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigUcrBgType:
	if (!getUcrBg (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigUInt16ArrayType:
	if (!getUInt16Array (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigUInt32ArrayType:
	if (!getUInt32Array (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigUInt64ArrayType:
	if (!getUInt64Array (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigUInt8ArrayType:
	if (!getUInt8Array (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigViewingConditionsType:
	if (!getViewingCondition (fp, profile, &base,
				   &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigXYZType:
	if (!getXYZ (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigNamedColor2Type:
	if (!getNamedColor2 (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      case icSigCrdInfoType:
	if (!getCrdInfo (fp, profile, &base, &(profile->tags.tag[i])))
	  return NULL;
	break;
      default:
	break;
    }
  }

  fclose (fp);
  profile->highval = 255;

  return profile;
}

static int
getheader (FILE *fp, icProfile *profile)
{
  long length;

  fseek (fp, (size_t)0, SEEK_END);
  length = ftell (fp);
  fseek (fp, (size_t)0, SEEK_SET);

  if (!gcmmget32 (fp, &(profile->header.size)))
    return false;

  if (profile->header.size != length)
    return false;

  if (!gcmmget32 (fp, &(profile->header.cmmId)))
    return false;

  (void)gcmmCMMinfo (profile->header.cmmId);

  if (!gcmmget32 (fp, &(profile->header.version)))
    return false;

  if (profile->header.version & 0x0000FFFF)
    fprintf (stderr, "warning: supposed 0's arn't\n");

  if (!gcmmget32 (fp, &(profile->header.deviceClass)))
    return false;

  switch (profile->header.deviceClass)
  {
    case icSigInputClass:
    case icSigDisplayClass:
    case icSigOutputClass:
    case icSigLinkClass:
    case icSigAbstractClass:
    case icSigColorSpaceClass:
    case icSigNamedColorClass:
      break;
    default:
      return false;
  }

  if (!gcmmget32 (fp, &(profile->header.colorSpace)))
    return false;

  switch (profile->header.colorSpace)
  {
    case icSigXYZData:
    case icSigLabData:
    case icSigLuvData:
    case icSigYCbCrData:
    case icSigYxyData:
    case icSigRgbData:
    case icSigGrayData:
    case icSigHsvData:
    case icSigHlsData:
    case icSigCmykData:
    case icSigCmyData:
    case icSig2colorData:
    case icSig3colorData:
    case icSig4colorData:
    case icSig5colorData:
    case icSig6colorData:
    case icSig7colorData:
    case icSig8colorData:
    case icSig9colorData:
    case icSig10colorData:
    case icSig11colorData:
    case icSig12colorData:
    case icSig13colorData:
    case icSig14colorData:
    case icSig15colorData:
      break;
    default:
      fprintf (stderr, "warning: unknown device color space\n");
  }

  if (!gcmmget32 (fp, &(profile->header.pcs)))
    return false;

  if (profile->header.pcs != icSigXYZData
      && profile->header.pcs != icSigLabData)
    return false;

  if (!gcmmget16 (fp, &(profile->header.date.year)))
    return false;

  if (!gcmmget16 (fp, &(profile->header.date.month)))
    return false;

  if (!gcmmget16 (fp, &(profile->header.date.day)))
    return false;

  if (!gcmmget16 (fp, &(profile->header.date.hours)))
    return false;

  if (!gcmmget16 (fp, &(profile->header.date.minutes)))
    return false;

  if (!gcmmget16 (fp, &(profile->header.date.seconds)))
    return false;

  if (!gcmmget32 (fp, &(profile->header.magic)))
    return false;

  if (!gcmmget32 (fp, &(profile->header.platform)))
    return false;

  (void)gcmmPlatformInfo (profile->header.platform);

  if (!gcmmget32 (fp, &(profile->header.flags)))
    return false;

  if (profile->header.version <= 0x02100000 &&
      (profile->header.flags & 0x0000FFFF))
    return false;

  if (!gcmmget32 (fp, &(profile->header.manufacturer)))
    return false;

  (void)gcmmManufacturerInfo (profile->header.manufacturer);

  if (!gcmmget32 (fp, &(profile->header.model)))
    return false;

  (void)gcmmModelInfo (profile->header.model);

  if (!gcmmget64 (fp, &(profile->header.attributes)))
    return false;

  if (profile->header.version <= 0x02100000 &&
      (profile->header.attributes & ~0x03L))
    return false;

  if (!gcmmget32 (fp, &(profile->header.renderingIntent)))
    return false;

  switch (profile->header.renderingIntent)
  {
    case icPerceptual:
    case icRelativeColorimetric:
    case icSaturation:
    case icAbsoluteColorimetric:
      break;
    default:
      return false;
  }

  /*
   * PCS Illuminant
   * (ColorSpace illuminant needs a tag.)
   * must be (currently) (0.9642,1.0000,0.8249) = D50
   */

  if (!gcmmget32 (fp, &(profile->header.illuminant.X)))
    return false;

  if (profile->header.illuminant.X != 0x0000F6B8
      && profile->header.illuminant.X != 0x0000F6D4
      && profile->header.illuminant.X != 0x0000F6D5)
    return false;

  if (!gcmmget32 (fp, &(profile->header.illuminant.Y)))
    return false;

  if (profile->header.illuminant.Y != 0x00010000)
    return false;

  if (!gcmmget32 (fp, &(profile->header.illuminant.Z)))
    return false;

  if (profile->header.illuminant.Z != 0x0000D2F7
      && profile->header.illuminant.Z != 0x0000D32b
      && profile->header.illuminant.Z != 0x0000D32c)
    return false;

  if (!gcmmget32 (fp, &(profile->header.creator)))
    return false;

  if (!gcmmgetsstr (fp, profile->header.reserved, 44))
    return false;

  return true;
}

static int
gettable (FILE *fp, icProfile *profile)
{
  int i;

  if (!gcmmget32 (fp, &(profile->tags.count)))
    return false;

  if ((profile->tags.tag
       = (icTag *)mkmem (profile->tags.count * sizeof (icTag))) == NULL)
    return false;

  for (i = 0; i < profile->tags.count; i++)
  {
    if (!gcmmget32 (fp, &(profile->tags.tag[i].sig)))
      return false;

    if (!gcmmget32 (fp, &(profile->tags.tag[i].offset)))
      return false;

    if (!gcmmget32 (fp, &(profile->tags.tag[i].size)))
      return false;
  }

  return true;
}

static void *
mkmem (size_t size)
{
  void *ptr;

  ptr = calloc (size, (size_t)1);
  if (!ptr)
    fprintf (stderr, "Virtual memory exhausted\n");

  return ptr;
}

static int
getCrdInfo (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icCrdInfoType *Crd = NULL;

  if (!(Crd = (icCrdInfoType *)mkmem (sizeof (icCrdInfoType))))
    return false;
  
  tag->data = Crd;
  Crd->base.sig = base->sig;
  Crd->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Crd->info.psnamecount)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Crd->info.psname), Crd->info.psnamecount))
    return false;

  if (!gcmmget32 (fp, &(Crd->info.rintent0count)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Crd->info.rintent0), Crd->info.rintent0count))
    return false;

  if (!gcmmget32 (fp, &(Crd->info.rintent1count)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Crd->info.rintent1), Crd->info.rintent1count))
    return false;

  if (!gcmmget32 (fp, &(Crd->info.rintent2count)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Crd->info.rintent2), Crd->info.rintent2count))
    return false;

  if (!gcmmget32 (fp, &(Crd->info.rintent3count)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Crd->info.rintent3), Crd->info.rintent3count))
    return false;

  return true;
}

static int
getCurve (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icCurveType *Curve;
  int i;

  if (!(Curve = (icCurveType *)mkmem (sizeof (icCurveType))))
    return false;
  
  tag->data = Curve;
  Curve->base.sig = base->sig;
  Curve->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Curve->curve.count)))
    return false;

  if ((Curve->curve.data
       = (icUInt16Array)mkmem (sizeof (icUInt16Number) * Curve->curve.count))
      == NULL)
    return false;

  for (i = 0; i < Curve->curve.count; i++)
    if (!gcmmget16 (fp, &(Curve->curve.data[i])))
      return false;

  return true;
}

static int
getData (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icDataType *Data;

  if ((Data = (icDataType *)mkmem (sizeof (icDataType))) == NULL)
    return false;

  tag->data = Data;
  Data->base.sig = base->sig;
  Data->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Data->data.dataFlag)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Data->data.data), tag->size - 12))
    return false;

  return true;
}

static int
getDateTime (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icDateTimeType *Time;

  if (!(Time = (icDateTimeType *)mkmem (sizeof (icDateTimeType))))
    return false;

  tag->data = Time;
  Time->base.sig = base->sig;
  Time->base.reserved = base->reserved;

  if (!gcmmget32 (fp, (void *)&(Time->date.year)))
    return false;
  if (!gcmmget32 (fp, (void *)&(Time->date.month)))
    return false;
  if (!gcmmget32 (fp, (void *)&(Time->date.day)))
    return false;
  if (!gcmmget32 (fp, (void *)&(Time->date.hours)))
    return false;
  if (!gcmmget32 (fp, (void *)&(Time->date.minutes)))
    return false;
  if (!gcmmget32 (fp, (void *)&(Time->date.seconds)))
    return false;

  return true;
}

static int
getLut16 (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icLut16Type *Lut;
  int i, limit;

  if ((Lut = (icLut16Type *)mkmem (sizeof (icLut16Type))) == NULL)
    return false;

  tag->data = Lut;
  Lut->base.sig = base->sig;
  Lut->base.reserved = base->reserved;

  if (!gcmmget8 (fp, &(Lut->lut.inputChan)))
    return false;

  if (!gcmmget8 (fp, &(Lut->lut.outputChan)))
    return false;

  if (!gcmmget8 (fp, &(Lut->lut.clutPoints)))
    return false;

  if (!gcmmget8 (fp, &(Lut->lut.pad)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e00)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e01)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e02)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e10)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e11)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e12)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e20)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e21)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e22)))
    return false;

  if (!gcmmget16 (fp, &(Lut->lut.inputEnt)))
    return false;

  if (!gcmmget16 (fp, &(Lut->lut.outputEnt)))
    return false;

  limit = Lut->lut.inputChan * Lut->lut.inputEnt;

  if ((Lut->lut.inputTable
       = (icUInt16Number *)mkmem (limit * sizeof (icUInt16Number))) == NULL)
    return false;

  for (i=0; i<limit; i++)
    if (!gcmmget16 (fp, &(Lut->lut.inputTable[i])))
      return false;

  limit = Lut->lut.clutPoints;
  for (i=1; i<Lut->lut.inputChan; i++)
    limit *= Lut->lut.clutPoints;
  limit *= Lut->lut.outputChan;

  if ((Lut->lut.clutTable
       = (icUInt16Number *)mkmem (limit * sizeof (icUInt16Number))) == NULL)
    return false;

  for (i=0; i<limit; i++)
    if (!gcmmget16 (fp, &(Lut->lut.clutTable[i])))
      return false;

  limit = Lut->lut.outputChan * Lut->lut.outputEnt;

  if ((Lut->lut.outputTable
       = (icUInt16Number *)mkmem (limit * sizeof (icUInt16Number))) == NULL)
    return false;

  for (i=0; i<limit; i++)
    if (!gcmmget16 (fp, &(Lut->lut.outputTable[i])))
      return false;

  return true;
}

static int
getLut8 (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icLut8Type *Lut;
  int i, limit;

  if ((Lut = (icLut8Type *)mkmem (sizeof (icLut8Type))) == NULL)
    return false;

  tag->data = Lut;
  Lut->base.sig = base->sig;
  Lut->base.reserved = base->reserved;

  if (!gcmmget8 (fp, &(Lut->lut.inputChan)))
    return false;

  if (!gcmmget8 (fp, &(Lut->lut.outputChan)))
    return false;

  if (!gcmmget8 (fp, &(Lut->lut.clutPoints)))
    return false;

  if (!gcmmget8 (fp, &(Lut->lut.pad)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e00)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e01)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e02)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e10)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e11)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e12)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e20)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e21)))
    return false;

  if (!gcmmget32 (fp, &(Lut->lut.e22)))
    return false;

  limit = Lut->lut.inputChan * 256;

  if ((Lut->lut.inputTable
       = (icUInt8Number *)mkmem (limit * sizeof (icUInt8Number))) == NULL)
    return false;

  for (i=0; i<limit; i++)
    if (!gcmmget8 (fp, &(Lut->lut.inputTable[i])))
      return false;

  limit = Lut->lut.clutPoints;
  for (i=1; i<Lut->lut.inputChan; i++)
    limit *= Lut->lut.clutPoints;
  limit *= Lut->lut.outputChan;

  if ((Lut->lut.clutTable
       = (icUInt8Number *)mkmem (limit * sizeof (icUInt8Number))) == NULL)
    return false;

  for (i=0; i<limit; i++)
    if (!gcmmget8 (fp, &(Lut->lut.clutTable[i])))
      return false;

  limit = Lut->lut.outputChan * 256;

  if ((Lut->lut.outputTable
       = (icUInt8Number *)mkmem (limit * sizeof (icUInt8Number))) == NULL)
    return false;

  for (i=0; i<limit; i++)
    if (!gcmmget8 (fp, &(Lut->lut.outputTable[i])))
      return false;

  return true;
}

static int
getMeasurement (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icMeasurementType *Measurement;

  if ((Measurement
       = (icMeasurementType *)mkmem (sizeof (icMeasurementType))) == NULL)
    return false;

  Measurement->base.sig = base->sig;
  Measurement->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Measurement->measurement.stdObserver)))
    return false;

  if (!gcmmget32 (fp, &(Measurement->measurement.backing.X)))
    return false;

  if (!gcmmget32 (fp, &(Measurement->measurement.backing.Y)))
    return false;

  if (!gcmmget32 (fp, &(Measurement->measurement.backing.Z)))
    return false;

  if (!gcmmget32 (fp, &(Measurement->measurement.geometry)))
    return false;

  if (!gcmmget32 (fp, &(Measurement->measurement.flare)))
    return false;

  if (!gcmmget32 (fp, &(Measurement->measurement.illuminant)))
    return false;

  return true;
}

static int
getNamedColor2 (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icNamedColor2Type *Namedcolor;
  int i, j;

  if ((Namedcolor
       = (icNamedColor2Type *)mkmem (sizeof (icNamedColor2Type))) == NULL)
    return false;

  tag->data = Namedcolor;
  Namedcolor->base.sig = base->sig;
  Namedcolor->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Namedcolor->ncolor.vendorFlag)))
    return false;

  if (!gcmmget32 (fp, &(Namedcolor->ncolor.count)))
    return false;

  if (!gcmmget32 (fp, &(Namedcolor->ncolor.nDeviceCoords)))
    return false;

  if (!gcmmgetsstr (fp, Namedcolor->ncolor.prefix, (size_t)32))
    return false;

  if (!gcmmgetsstr (fp, Namedcolor->ncolor.suffix, (size_t)32))
    return false;

  if ((Namedcolor->ncolor.name
       = (icColorName *)mkmem (Namedcolor->ncolor.count
			       * sizeof (icColorName))) == NULL)
    return false;

  for (i = 0; i < Namedcolor->ncolor.count; i++)
  {
    int ncoords = gcmmncomponents (profile->header.pcs);
    if (!gcmmgetsstr (fp, Namedcolor->ncolor.name[i].root, (size_t)32))
      return false;

    for (j = 0; j < ncoords; j++)
      if (!gcmmget16 (fp, (void *)&(Namedcolor->ncolor.name[i].pcsCoords[j])))
	return false;

    for (j = 0; j < Namedcolor->ncolor.nDeviceCoords; j++)
      if (!gcmmget16 (fp, (void *)&(Namedcolor->ncolor.name[i].deviceCoords[j])))
	return false;
  }

  return true;
}

static int
getProfileSequenceDesc (FILE *fp, icProfile *profile, icTagBase *base,
			icTag *tag)
{
  icProfileSequenceDescType *Sequence;
  int i;

  if ((Sequence = (icProfileSequenceDescType *)
       mkmem (sizeof (icProfileSequenceDescType))) == NULL)
    return false;

  tag->data = Sequence;
  Sequence->base.sig = base->sig;
  Sequence->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Sequence->desc.count)))
    return false;

  if (!(Sequence->desc.data
       = (icDescStruct *)mkmem (Sequence->desc.count * sizeof (icDescStruct))))
    return false;

  for (i = 0; i < Sequence->desc.count; i++)
  {
    if (!gcmmget32 (fp, &(Sequence->desc.data[i].deviceMfg)))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].deviceModel)))
      return false;

    if (!gcmmget64 (fp, &(Sequence->desc.data[i].attributes)))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].technology)))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].deviceMfgDesc.count)))
      return false;

    if (!gcmmgetstr (fp, (void *)&(Sequence->desc.data[i].deviceMfgDesc.desc),
		   Sequence->desc.data[i].deviceMfgDesc.count))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].deviceMfgDesc.ucLangCode)))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].deviceMfgDesc.ucCount)))
      return false;

    if (!gcmmgetstr (fp, (void *)&(Sequence->desc.data[i].deviceMfgDesc.ucDesc),
		   Sequence->desc.data[i].deviceMfgDesc.ucCount
		   * sizeof (icInt16Number)))
      return false;

    if (!gcmmget16 (fp, &(Sequence->desc.data[i].deviceMfgDesc.scCode)))
      return false;

    if (!gcmmget8 (fp, &(Sequence->desc.data[i].deviceMfgDesc.scCount)))
      return false;

    if (!gcmmgetstr (fp, (void *)&(Sequence->desc.data[i].deviceMfgDesc.scDesc),
		   Sequence->desc.data[i].deviceMfgDesc.scCount))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].modelDesc.count)))
      return false;

    if (!gcmmgetstr (fp, (void *)&(Sequence->desc.data[i].modelDesc.desc),
		   Sequence->desc.data[i].modelDesc.count))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].modelDesc.ucLangCode)))
      return false;

    if (!gcmmget32 (fp, &(Sequence->desc.data[i].modelDesc.ucCount)))
      return false;

    if (!gcmmgetstr (fp, (void *)&(Sequence->desc.data[i].modelDesc.ucDesc),
		   Sequence->desc.data[i].modelDesc.ucCount
		   * sizeof (icUInt16Number)))
      return false;

    if (!gcmmget16 (fp, &(Sequence->desc.data[i].modelDesc.scCode)))
      return false;

    if (!gcmmget8 (fp, &(Sequence->desc.data[i].modelDesc.scCount)))
      return false;

    if (!gcmmgetstr (fp, (void *)&(Sequence->desc.data[i].modelDesc.scDesc),
		   Sequence->desc.data[i].modelDesc.scCount))
      return false;
  }

  return true;
}

static int
getS15Fixed16Array (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icS15Fixed16ArrayType *Array;
  int i, limit = tag->size - 8;

  if ((Array
       = (icS15Fixed16ArrayType *)mkmem (sizeof (icS15Fixed16ArrayType)))
      == NULL)
    return false;

  tag->data = Array;
  Array->base.sig = base->sig;
  Array->base.reserved = base->reserved;

  if ((Array->data
       = (icS15Fixed16Array)mkmem (limit * sizeof (icS15Fixed16Number)))
      == NULL)
    return false;

  for (i = 0; i < limit; i++)
    if (!gcmmget32 (fp, &(Array->data[i])))
      return false;

  return true;
}

static int
getScreening (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icScreeningType *Screen;
  int i;

  if ((Screen = (icScreeningType *)mkmem (sizeof (icScreeningType))) == NULL)
    return false;

  tag->data = Screen;
  Screen->base.sig = base->sig;
  Screen->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Screen->screen.screeningFlag)))
    return false;

  if (!gcmmget32 (fp, &(Screen->screen.channels)))
    return false;

  if ((Screen->screen.data
       = (icScreeningData *)mkmem (sizeof(icScreeningData))) == NULL)
    return false;

  for (i = 0; i < Screen->screen.channels; i++)
  {
    if (!gcmmget32 (fp, &(Screen->screen.data[i].frequency)))
      return false;

    if (!gcmmget32 (fp, &(Screen->screen.data[i].angle)))
      return false;

    if (!gcmmget32 (fp, &(Screen->screen.data[i].spotShape)))
      return false;
  }
  return 1;
}

static int
getSignature (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icSignatureType *Sig;

  if ((Sig = (icSignatureType *)mkmem (sizeof (icSignatureType))) == NULL)
    return false;

  tag->data = Sig;
  Sig->base.sig = base->sig;
  Sig->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Sig->signature)))
    return false;
  return true;
}

static int
getTextDescription (FILE *fp, icProfile *profile, icTagBase *base,
		    icTag *tag)
{
  icTextDescriptionType *Text;
  char *tmp = 0;
  if ((Text = (icTextDescriptionType *)mkmem (sizeof (icTextDescriptionType)))
      == NULL)
    return false;

  tag->data = Text;
  Text->base.sig = base->sig;
  Text->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Text->desc.count)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Text->desc.desc), Text->desc.count))
    return false;

  if (!gcmmget32 (fp, &(Text->desc.ucLangCode)))
    return false;

  if (!gcmmget32 (fp, &(Text->desc.ucCount)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Text->desc.ucDesc), Text->desc.ucCount
		 * sizeof (icInt16Number)))
    return false;

  if (!gcmmget16 (fp, &(Text->desc.scCode)))
    return false;

  if (!gcmmget8 (fp, &(Text->desc.scCount)))
    return false;

  if (!gcmmgetstr (fp, (void *)(&tmp), Text->desc.scCount))
    return false;
  strncpy (Text->desc.scDesc, tmp, 66);
  if (tmp)
    free (tmp);
  return true;
}

static int
getText (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icTextType *Text;
  int limit = tag->size - 8;

  if ((Text = (icTextType *)mkmem (sizeof (icTextType))) == NULL)
    return false;

  tag->data = Text;
  Text->base.sig = base->sig;
  Text->base.reserved = base->reserved;

  if (!gcmmgetstr (fp, (void *)&(Text->data), limit))
    return false;

  return true;
}

static int
getU16Fixed16Array (FILE *fp, icProfile *profile, icTagBase *base,
		    icTag *tag)
{
  icU16Fixed16ArrayType *Array;
  int i, limit = tag->size - 8;

  if ((Array
       = (icU16Fixed16ArrayType *)mkmem (sizeof (icU16Fixed16ArrayType)))
      == NULL)
    return false;

  tag->data = Array;
  Array->base.sig = base->sig;
  Array->base.reserved = base->reserved;

  if ((Array->data
       = (icU16Fixed16Number *)mkmem (limit * sizeof (icU16Fixed16Number)))
      == NULL)
    return false;

  for (i = 0; i < limit; i++)
    if (!gcmmget32 (fp, &(Array->data[i])))
      return false;

  return true;
}

static int
getUcrBg (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icUcrBgType *Ucrbg;
  int i, limit;

  if ((Ucrbg = (icUcrBgType *)mkmem (sizeof (icUcrBgType))) == NULL)
    return false;

  tag->data = Ucrbg;
  Ucrbg->base.sig = base->sig;
  Ucrbg->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Ucrbg->data.ucr.count)))
    return false;

  if ((Ucrbg->data.ucr.curve
       = (icUInt16Number *)mkmem (sizeof(icUInt16Number))) == NULL)
    return false;

  for (i = 0; i < Ucrbg->data.ucr.count; i++)
    if (!gcmmget16 (fp, &(Ucrbg->data.ucr.curve[i])))
      return false;

  if (!gcmmget32 (fp, &(Ucrbg->data.bg.count)))
    return false;

  if ((Ucrbg->data.bg.curve
       = (icUInt16Number *)mkmem (sizeof(icUInt16Number))) == NULL)
    return false;

  for (i = 0; i < Ucrbg->data.bg.count; i++)
    if (!gcmmget16 (fp, &(Ucrbg->data.bg.curve[i])))
      return false;

  limit = tag->size - 16 - Ucrbg->data.ucr.count - Ucrbg->data.bg.count;

  if (!gcmmgetstr (fp, (void *)&(Ucrbg->data.string), limit))
    return false;

  return true;
}

static int
getUInt16Array (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icUInt16ArrayType *Array;
  int i, limit = tag->size - 8;

  if ((Array
       = (icUInt16ArrayType *)mkmem (sizeof (icUInt16ArrayType))) == NULL)
    return false;

  tag->data = Array;
  Array->base.sig = base->sig;
  Array->base.reserved = base->reserved;

  if ((Array->data
       = (icUInt16Number *)mkmem (limit * sizeof (icUInt16Number))) == NULL)
    return false;

  for (i = 0; i<limit; i++)
    if (!gcmmget16 (fp, &(Array->data[i])))
      return false;

  return true;
}

static int
getUInt32Array (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icUInt32ArrayType *Array;
  int i, limit = tag->size - 8;

  if (!(Array = (icUInt32ArrayType *)mkmem (sizeof (icUInt32ArrayType))))
    return false;

  tag->data = Array;
  Array->base.sig = base->sig;
  Array->base.reserved = base->reserved;

  if ((Array->data
       = (icUInt32Number *)mkmem (limit * sizeof (icUInt32Number))) == NULL)
    return false;

  for (i = 0; i<limit; i++)
    if (!gcmmget32 (fp, &(Array->data[i])))
      return false;

  return true;
}

static int
getUInt64Array (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icUInt64ArrayType *Array;
  int i, limit = tag->size - 8;

  if ((Array
       = (icUInt64ArrayType *)mkmem (sizeof (icUInt64ArrayType))) == NULL)
    return false;

  tag->data = Array;
  Array->base.sig = base->sig;
  Array->base.reserved = base->reserved;

  if ((Array->data
       = (icUInt64Number *)mkmem (limit * sizeof (icUInt64Number))) == NULL)
    return false;

  for (i = 0; i<limit; i++)
    if (!gcmmget64 (fp, &(Array->data[i])))
      return false;

  return true;
}

static int
getUInt8Array (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icUInt8ArrayType *Array;
  int limit = tag->size - 8;

  if ((Array
       = (icUInt8ArrayType *)mkmem (sizeof (icUInt8ArrayType))) == NULL)
    return false;

  tag->data = Array;
  Array->base.sig = base->sig;
  Array->base.reserved = base->reserved;

  if (!gcmmgetstr (fp, (void *)&(Array->data), limit))
    return false;

  return true;
}

static int
getViewingCondition (FILE *fp, icProfile *profile, icTagBase *base,
		     icTag *tag)
{
  icViewingConditionType *View;

  if ((View
       = (icViewingConditionType *)mkmem (sizeof (icViewingConditionType)))
      == NULL)
    return false;

  tag->data = View;
  View->base.sig = base->sig;
  View->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(View->view.illuminant.X)))
    return false;

  if (!gcmmget32 (fp, &(View->view.illuminant.Y)))
    return false;

  if (!gcmmget32 (fp, &(View->view.illuminant.Z)))
    return false;

  if (!gcmmget32 (fp, &(View->view.surround.X)))
    return false;

  if (!gcmmget32 (fp, &(View->view.surround.Y)))
    return false;

  if (!gcmmget32 (fp, &(View->view.surround.Z)))
    return false;

  if (!gcmmget32 (fp, &(View->view.stdIlluminant)))
    return false;

  return true;
}

static int
getXYZ (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icXYZType *Xyz;
  int i, limit;

  if ((Xyz = (icXYZType *)mkmem (sizeof (icXYZType))) == NULL)
    return false;

  tag->data = Xyz;
  Xyz->base.sig = base->sig;
  Xyz->base.reserved = base->reserved;

  limit = (tag->size - 8) / (3 * sizeof (icS15Fixed16Number));

  if ((Xyz->data = (icXYZNumber *)mkmem (limit * sizeof (icXYZNumber)))
      == NULL)
    return false;

  for (i = 0; i < limit; i++)
  {
    if (!gcmmget32 (fp, &(Xyz->data[i].X)))
      return false;

    if (!gcmmget32 (fp, &(Xyz->data[i].Y)))
      return false;

    if (!gcmmget32 (fp, &(Xyz->data[i].Z)))
      return false;
  }

  return true;
}

static int
getNamedColor (FILE *fp, icProfile *profile, icTagBase *base, icTag *tag)
{
  icNamedColorType *Namedcolor;
  int i, j, ncoords;

  ncoords = gcmmncomponents (profile->header.colorSpace);

  if ((Namedcolor
       = (icNamedColorType *)mkmem (sizeof (icNamedColorType))) == NULL)
    return false;

  tag->data = Namedcolor;
  Namedcolor->base.sig = base->sig;
  Namedcolor->base.reserved = base->reserved;

  if (!gcmmget32 (fp, &(Namedcolor->ncolor.vendorFlag)))
    return false;

  if (!gcmmget32 (fp, &(Namedcolor->ncolor.count)))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Namedcolor->ncolor.prefix), (size_t)32))
    return false;

  if (!gcmmgetstr (fp, (void *)&(Namedcolor->ncolor.suffix), (size_t)32))
    return false;

  if ((Namedcolor->ncolor.name
       = (icColorName *)mkmem (Namedcolor->ncolor.count
			       * sizeof (icColorName))) == NULL)
    return false;

  for (i = 0; i < Namedcolor->ncolor.count; i++)
  {
    if (!gcmmgetstr (fp, (void *)&(Namedcolor->ncolor.name[i].root), (size_t)32))
      return false;

    for (j = 0; j < ncoords; j++)
      if (!gcmmget32 (fp, (void *)&(Namedcolor->ncolor.name[i].deviceCoords[j])))
	return false;
  }

  return true;
}

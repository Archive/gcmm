/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "gcmm.h"

#ifndef WORDS_BIGENDIAN
static void swap8 (icUInt8Number *a, icUInt8Number *b);
icUInt16Number gcmmswap16 (icUInt16Number val);
icUInt32Number gcmmswap32 (icUInt32Number val);
icUInt64Number gcmmswap64 (icUInt64Number val);
#endif /* little endian */

icUInt8Number gcmmbcd8str (char *str);
icUInt16Number gcmmbcd16str (char *str);
icUInt32Number gcmmbcd32str (char *str);
char *gcmmstrbcd8 (icUInt8Number val);
char *gcmmstrbcd16 (icUInt16Number val);
char *gcmmstrbcd32 (icUInt32Number val);
double gcmmdu8f8 (icU8Fixed8Number val);
icU8Fixed8Number gcmmu8f8d (double val);
double gcmmds15f16 (icS15Fixed16Number val);
icS15Fixed16Number gcmms15f16d (double val);
double gcmmdu16f16 (icU16Fixed16Number val);
icU16Fixed16Number gcmmu16f16d (double val);
icUInt8Number gcmmLstar8encode (double val);
icUInt16Number gcmmLstar16encode (double val);
double gcmmLstar8decode (icUInt8Number val);
double gcmmLstar16decode (icUInt16Number val);
icUInt8Number gcmmencodeabstar8 (double val);
icUInt16Number gcmmencodeabstar16 (double val);
double gcmmdecodeabstar8 (icUInt8Number val);
double gcmmdecodeabstar16 (icUInt16Number val);
double gcmmastar2std (double val);
double gcmmastar2ic (double val);
double gcmmbstar2std (double val);
double gcmmbstar2ic (double val);
char *gcmmstricdate (icDateTimeNumber *date);
struct tm *gcmmtmicdate (icDateTimeNumber *date);
icDateTimeNumber *gcmmicdatetm (struct tm *tm);
int gcmmget8 (FILE *fp, icUInt8Number *val);
int gcmmget16 (FILE *fp, icUInt16Number *val);
int gcmmget32 (FILE *fp, icUInt32Number *val);
int gcmmget64 (FILE *fp, icUInt64Number *val);
int gcmmgetstr (FILE *fp, icUInt8Array *ptr, icUInt32Number size);
int gcmmgetsstr (FILE *fp, icUInt8Array ptr, icUInt32Number size);
int gcmmput8 (FILE *fp, icUInt8Number val);
int gcmmput16 (FILE *fp, icUInt16Number val);
int gcmmput32 (FILE *fp, icUInt32Number val);
int gcmmput64 (FILE *fp, icUInt64Number val);
int gcmmputstr (FILE *fp, icUInt8Array ptr, icUInt32Number size);
int gcmmisicc (char *name);
char *gcmmstrversion (icUInt32Number vers);
icUInt32Number gcmmversionstr (char *vers);
int gcmmncomponents (icColorSpaceSignature cs);


#ifndef WORDS_BIGENDIAN
static void
swap8 (icUInt8Number *a, icUInt8Number *b)
{
  icUInt8Number temp;

  temp = *a;
  *a = *b;
  *b = temp;
}

icUInt16Number
gcmmswap16 (icUInt16Number val)
{
  union
  {
    icUInt16Number val;
    struct
    {
      icUInt8Number b0;
      icUInt8Number b1;
    }b;
  }res;

  res.val = val;
  swap8 (&(res.b.b0), &(res.b.b1));

  return res.val;
}

icUInt32Number
gcmmswap32 (icUInt32Number val)
{
  union
  {
    icUInt32Number val;
    struct
    {
      icUInt8Number b0;
      icUInt8Number b1;
      icUInt8Number b2;
      icUInt8Number b3;
    }b;
  }res;

  res.val = val;
  swap8 (&(res.b.b0), &(res.b.b3));
  swap8 (&(res.b.b1), &(res.b.b2));

  return res.val;
}

icUInt64Number
gcmmswap64 (icUInt64Number val)
{
  union
  {
    icUInt64Number val;
    struct
    {
      icUInt8Number b0;
      icUInt8Number b1;
      icUInt8Number b2;
      icUInt8Number b3;
      icUInt8Number b4;
      icUInt8Number b5;
      icUInt8Number b6;
      icUInt8Number b7;
    }b;
  }res;

  res.val = val;
  swap8 (&(res.b.b0), &(res.b.b7));
  swap8 (&(res.b.b1), &(res.b.b6));
  swap8 (&(res.b.b2), &(res.b.b5));
  swap8 (&(res.b.b3), &(res.b.b4));

  return res.val;
}
#endif /* little endian */

char *
gcmmstrbcd8 (icUInt8Number val)
{
  static char str[3];
  union
  {
    icUInt8Number val;
    struct
    {
      unsigned int nib0:4;
      unsigned int nib1:4;
    }n;
  }u;

  u.val = val;
  if (u.n.nib0 > 9 || u.n.nib1 > 9)
    return NULL;

  str[0] = u.n.nib0 + '0';
  str[1] = u.n.nib1 + '0';
  str[2] = '\0';

  return str;
}

icUInt8Number
gcmmbcd8str (char *str)
{
  union
  {
    icUInt8Number val;
    struct
    {
      int nib0:4;
      int nib1:4;
    }n;
  }u;

  if (str[0] < '0' || str[0] > '9' || str[1] < '0' || str[1] > '9')
    return 0xFF;

  u.n.nib0 = str[0] - '0';
  u.n.nib1 = str[1] - '0';

  return u.val;
}

char *
gcmmstrbcd16 (icUInt16Number val)
{
  static char str[5];
  union
  {
    icUInt16Number val;
    struct
    {
      unsigned int nib0:4;
      unsigned int nib1:4;
      unsigned int nib2:4;
      unsigned int nib3:4;
    }n;
  }u;

  u.val = val;
  if (u.n.nib0 > 9 || u.n.nib1 > 9 || u.n.nib2 > 9 || u.n.nib3 > 9)
    return NULL;

  str[0] = u.n.nib0 + '0';
  str[1] = u.n.nib1 + '0';
  str[2] = u.n.nib2 + '0';
  str[3] = u.n.nib3 + '0';
  str[4] = '\0';

  return str;
}

icUInt16Number
gcmmbcd16str (char *str)
{
  union
  {
    icUInt16Number val;
    struct
    {
      int nib0:4;
      int nib1:4;
      int nib2:4;
      int nib3:4;
    }n;
  }u;

  if (str[0] < '0' || str[0] > '9' || str[1] < '0' || str[1] > '9'
      || str[2] < '0' || str[2] > '9' || str[3] < '0' || str[3] > '9')
    return 0xFFFF;

  u.n.nib0 = str[0] - '0';
  u.n.nib1 = str[1] - '0';
  u.n.nib2 = str[2] - '0';
  u.n.nib3 = str[3] - '0';

  return u.val;
}

char *
gcmmstrbcd32 (icUInt32Number val)
{
  static char str[9];
  union
  {
    icUInt32Number val;
    struct
    {
      unsigned int nib7:4;
      unsigned int nib6:4;
      unsigned int nib5:4;
      unsigned int nib4:4;
      unsigned int nib3:4;
      unsigned int nib2:4;
      unsigned int nib1:4;
      unsigned int nib0:4;
    }n;
  }u;

  u.val = val;
  if (u.n.nib0 > 9 || u.n.nib1 > 9 || u.n.nib2 > 9 || u.n.nib3 > 9
      || u.n.nib4 > 9 || u.n.nib5 > 9 || u.n.nib6 > 9 || u.n.nib7 > 9)
    return NULL;

  str[0] = u.n.nib0 + '0';
  str[1] = u.n.nib1 + '0';
  str[2] = u.n.nib2 + '0';
  str[3] = u.n.nib3 + '0';
  str[4] = u.n.nib4 + '0';
  str[5] = u.n.nib5 + '0';
  str[6] = u.n.nib6 + '0';
  str[7] = u.n.nib7 + '0';
  str[8] = '\0';

  return str;
}

icUInt32Number
gcmmbcd32str (char *str)
{
  union
  {
    icUInt32Number val;
    struct
    {
      int nib0:4;
      int nib1:4;
      int nib2:4;
      int nib3:4;
      int nib4:4;
      int nib5:4;
      int nib6:4;
      int nib7:4;
    }n;
  }u;

  if (str[0] < '0' || str[0] > '9' || str[1] < '0' || str[1] > '9'
      || str[2] < '0' || str[2] > '9' || str[3] < '0' || str[3] > '9'
      || str[4] < '0' || str[4] > '9' || str[5] < '0' || str[5] > '9'
      || str[6] < '0' || str[6] > '9' || str[7] < '0' || str[7] > '9')
    return 0xFFFFFFFF;

  u.n.nib0 = str[0] - '0';
  u.n.nib1 = str[1] - '0';
  u.n.nib2 = str[2] - '0';
  u.n.nib3 = str[3] - '0';
  u.n.nib4 = str[4] - '0';
  u.n.nib5 = str[5] - '0';
  u.n.nib6 = str[6] - '0';
  u.n.nib7 = str[7] - '0';

  return u.val;
}

double
gcmmdu8f8 (icU8Fixed8Number val)
{
  double res;

  res = (double)((unsigned long)val >> 8);
  res += (double)(val & 0xFF) / ((double)0xFF + 1);

  return res;
}

icU8Fixed8Number
gcmmu8f8d (double val)
{
  icU8Fixed8Number res;

  res = val;
  res <<= 8;
  val -= res;
  res += val;

  return res;
}

double
gcmmds15f16 (icS15Fixed16Number val)
{
  double res;

  res = (double)((signed long)val >> 16);
  res += (double)(val & 0xFFFF) / ((double)0xFFFF + 1);

  return res;
}

icS15Fixed16Number
gcmms15f16d (double val)
{
  icS15Fixed16Number res;

  res = val;
  res <<= 16;
  val -= res;
  res += val;

  return res;
}

double
gcmmdu16f16 (icU16Fixed16Number val)
{
  double res;

  res = (double)((unsigned long)val >> 16);
  res += (double)(val & 0xFFFF) / ((double)0xFFFF + 1);

  return res;
}

icU16Fixed16Number
gcmmu16f16d (double val)
{
  icU16Fixed16Number res;

  res = val;
  res <<= 16;
  val -= res;
  res += val;

  return res;
}

icUInt8Number
gcmmLstar8encode (double val)                            /* 0.0 < val < 100.0 */
{
  if (val < 0.0)            /* should we better warn?   */
    val = 0.0;              /* at least we can continue */
  if (val > 100.0)
    val = 100.0;
  return (icUInt8Number)((val * 2.55) + 0.5);
}

icUInt16Number
gcmmLstar16encode (double val)                           /* 0.0 < val < 100.0 */
{
  if (val < 0.0)
    val = 0.0;
  if (val > 100.0)
    val = 100.0;
  return (icUInt16Number)((val * 652.8) + 0.5);
}

double
gcmmLstar8decode (icUInt8Number val)
{
  double res = (double)val / 2.55;

  if (res < 0.0)
    res = 0.0;
  if (res > 100.0)
    res = 100.0;
  return res;
}

double
gcmmLstar16decode (icUInt16Number val)
{
  double res;

  res = ((val >> 8) * 100) / 255;
  res += ((double)(val & 0xFF) * 100.0) / 65280.0;
  return res;
}

icUInt8Number
gcmmencodeabstar8 (double val)                             /* -128 < 0 < +127 */
{
  return (icUInt8Number)((val + 128) + 0.5);
}

icUInt16Number
gcmmencodeabstar16 (double val)                            /* -128 < 0 < +127 */
{
  return (icUInt16Number)(((val + 128) * 256) + 0.5);
}

double
gcmmdecodeabstar8 (icUInt8Number val)
{
  return (double)val - 128.0;
}

double
gcmmdecodeabstar16 (icUInt16Number val)
{
  return ((double)val / 256.0) - 128.0;
}

double
gcmmastar2std (double val)
{
  return val * 500.0 / 127.0;
}

double
gcmmbstar2std (double val)
{
  return val * 200.0 / 127.0;
}

double
gcmmastar2ic (double val)
{
  if (val > 0.0)
    return val * (127.0 + 255.0/256.0) / 500.0;
  else
    return val * 127.0 / 500.0;
}

double
gcmmbstar2ic (double val)
{
  if (val > 0.0)
    return val * (127.0 + 255.0/256.0) / 200.0;
  else
    return val * 127.0 / 200.0;
}

char *
gcmmstricdate (icDateTimeNumber *date)
{
  static char buf[BUFSIZ];
  struct tm *tm;
  size_t size;

  tm = gcmmtmicdate (date);
  if (!tm)
    return NULL;

  size = strftime (buf, BUFSIZ, "%A, %x %X", tm);
  if (size == BUFSIZ)
    return NULL;
  return buf;
}

struct tm *
gcmmtmicdate (icDateTimeNumber *date)
{
  struct tm tm;
  time_t tim;

  if (date->seconds > 59)
    return NULL;
  tm.tm_sec = date->seconds;
  if (date->minutes > 59)
    return NULL;
  tm.tm_min = date->minutes;
  if (date->hours > 23)
    return NULL;
  tm.tm_hour = date->hours;
  if (date->day < 1 || date->day > 31)
    return NULL;
  tm.tm_mday = date->day;
  tm.tm_mon = date->month - 1;
  if (tm.tm_mon > 11)
    return NULL;
  tm.tm_year = date->year - 1900;
  tm.tm_wday = tm.tm_yday = tm.tm_isdst = 0;

  tim = mktime (&tm);
  if (tim == -1)
    return NULL;
  return localtime (&tim);
}

icDateTimeNumber *
gcmmicdatetm (struct tm *tm)
{
  static icDateTimeNumber date;

  if (tm->tm_sec > 59)
    return NULL;
  date.seconds = tm->tm_sec;
  if (tm->tm_min > 59)
    return NULL;
  date.minutes = tm->tm_min;
  if (tm->tm_hour > 23)
    return NULL;
  date.hours = tm->tm_hour;
  if (tm->tm_mday < 1 || tm->tm_mday > 31)
    return NULL;
  date.day = tm->tm_mday;
  date.month = tm->tm_mon + 1;
  if (tm->tm_mon > 11)
    return NULL;
  date.year = tm->tm_year + 1900;

  return &date;
}

int
gcmmget8 (FILE *fp, icUInt8Number *val)
{
  if (fread (val, sizeof (icUInt8Number), (size_t)1, fp) != 1)
    return false;

  return true;
}

int
gcmmget16 (FILE *fp, icUInt16Number *val)
{
  if (fread (val, sizeof (icUInt16Number), (size_t)1, fp) != 1)
    return false;
#ifndef WORDS_BIGENDIAN
  *val = gcmmswap16 (*val);
#endif

  return true;
}

int
gcmmget32 (FILE *fp, icUInt32Number *val)
{
  if (fread (val, sizeof (icUInt32Number), (size_t)1, fp) != 1)
    return false;
#ifndef WORDS_BIGENDIAN
  *val = gcmmswap32 (*val);
#endif

  return true;
}

int
gcmmget64 (FILE *fp, icUInt64Number *val)
{
  if (fread (val, sizeof (icUInt64Number), (size_t)1, fp) != 1)
    return false;
#ifndef WORDS_BIGENDIAN
  *val = gcmmswap64 (*val);
#endif

  return true;
}

int
gcmmgetstr (FILE *fp, icUInt8Array *ptr, icUInt32Number size)
{
  icUInt8Array p;
  int i;

  *ptr = NULL;
  if (!size)
    return true;

  p = (icUInt8Array)calloc (size, sizeof (icUInt8Number));
  if (!p)
    return false;
  for (i = 0; i < size; i++)
    if (!gcmmget8 (fp, &(p[i])))
    {
      free (p);
      return false;
    }
  *ptr = p;

  return true;
}

int
gcmmgetsstr (FILE *fp, icUInt8Array ptr, icUInt32Number size)
{
  int i;

  if (!size || !ptr)
    return true;

  for (i = 0; i < size; i++)
    if (!gcmmget8 (fp, &(ptr[i])))
      return false;

  return true;
}

int
gcmmput8 (FILE *fp, icUInt8Number val)
{
  if (fwrite (&val, sizeof (icUInt8Number), (size_t)1, fp) != 1)
    return false;

  return true;
}

int
gcmmput16 (FILE *fp, icUInt16Number val)
{
#ifndef WORDS_BIGENDIAN
  val = gcmmswap16 (val);
#endif
  if (fwrite (&val, sizeof (icUInt16Number), (size_t)1, fp) != 1)
    return false;

  return true;
}

int
gcmmput32 (FILE *fp, icUInt32Number val)
{
#ifndef WORDS_BIGENDIAN
  val = gcmmswap32 (val);
#endif
  if (fwrite (&val, sizeof (icUInt32Number), (size_t)1, fp) != 1)
    return false;

  return true;
}

int
gcmmputstr (FILE *fp, icUInt8Array ptr, icUInt32Number size)
{
  int i;

  if (!ptr)
    return true;

  for (i = 0; i < size; i++)
    if (!gcmmput8 (fp, ptr[i]))
      return false;

  return true;
}

int
gcmmput64 (FILE *fp, icUInt64Number val)
{
#ifndef WORDS_BIGENDIAN
  val = gcmmswap64 (val);
#endif
  if (fwrite (&val, sizeof (icUInt64Number), (size_t)1, fp) != 1)
    return false;

  return true;
}

int
gcmmisicc (char *name)
{
  FILE *fp;
  icUInt32Number val;

  fp = fopen (name, "r");
  if (!fp)
    return false;
  fseek (fp, (size_t)36, SEEK_SET);
  if (!gcmmget32(fp, &val))
  {
    fclose (fp);
    return false;
  }
  fclose (fp);
  return val == icMagicNumber;
}

char *
gcmmstrversion (icUInt32Number vers)
{
  icUInt8Number *p, *q;
  static char buf[BUFSIZ];

  p = (icUInt8Number *)(&vers);
  q = p + 1;
  sprintf (buf, "%s.%s\n", gcmmstrbcd8 (*p), gcmmstrbcd8 (*q));
  return buf;
}

icUInt32Number
gcmmversionstr (char *vers)
{
  char *p, *q, buf[8];

  strcpy (buf, "00000000");
  p = q = NULL;
  p = strtok (vers, ".");
  if (p)
    q = strtok (NULL, ".");
  if (p)
  {
    buf[0] = p[0];
    buf[1] = p[1];
  }
  if (q)
  {
    buf[2] = q[0];
    buf[3] = q[1];
  }

  return gcmmbcd32str (buf);
}

int
gcmmncomponents (icColorSpaceSignature cs)
{
  switch (cs)
  {
    case icSigGrayData:
      return 1;
    case icSig2colorData:
      return 2;
    case icSigXYZData:
    case icSigLabData:
    case icSigLuvData:
    case icSigYCbCrData:
    case icSigYxyData:
    case icSigRgbData:
    case icSigHsvData:
    case icSigHlsData:
    case icSigCmyData:
    case icSig3colorData:
      return 3;
    case icSigCmykData:
    case icSig4colorData:
      return 4;
    case icSig5colorData:
      return 5;
    case icSig6colorData:
      return 6;
    case icSig7colorData:
      return 7;
    case icSig8colorData:
      return 8;
    case icSig9colorData:
      return 9;
    case icSig10colorData:
      return 10;
    case icSig11colorData:
      return 11;
    case icSig12colorData:
      return 12;
    case icSig13colorData:
      return 13;
    case icSig14colorData:
      return 14;
    case icSig15colorData:
      return 15;
    default:
      return false;
  }
}

int
gcmmfindtag (icProfile *profile, icSignature sig)
{
  int i, limit = profile->tags.count;
  icTag *ptr = profile->tags.tag;

  for (i = 0; i < limit; i++)
    if (ptr[i].sig == sig)
      break;
  if (i == limit)
    return false;
  return (ptr[i].sig == sig) ? i : -1;
}

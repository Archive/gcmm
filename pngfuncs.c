/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <png.h>

static png_structp png;
static png_infop pnginfo;
static png_infop pngend;

int
getpng (char *filename, unsigned char ***rows, int *ht, int *wd)
{
  FILE *fp;
  int i;

  fp = fopen (filename, "rb");
  if (!fp)
    return 0;

  png = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png)
    return 0;

  pnginfo = png_create_info_struct (png);
  if (!pnginfo)
    return 0;

  pngend = png_create_info_struct (png);
  if (!pngend)
    return 0;

  if (setjmp (png->jmpbuf))
    return 0;

  png_init_io (png, fp);
  png_read_info (png, pnginfo);

  if (pnginfo->color_type != PNG_COLOR_TYPE_RGB)
    return 0;

  if (pnginfo->bit_depth == 16)
    png_set_strip_16 (png);

  if (pnginfo->interlace_type)
    return 0;

  png_read_update_info (png, pnginfo);

  *ht = pnginfo->height;
  *wd = pnginfo->width;
  *rows = (unsigned char **)calloc (*ht, sizeof (unsigned char *));
  if (!*rows)
    return 0;
  for (i=0; i<*ht; i++)
  {
    (*rows)[i] = (unsigned char *)calloc (*wd, sizeof (unsigned char) * 3);
    if (!(*rows)[i])
      return 0;
  }
  png_read_image (png, *rows);
  png_read_end (png, pngend);
  fclose (fp);

  return 1;
}

int
putpng (char *filename, unsigned char **rows, int ht, int wd)
{
  FILE *fp;

  fp = fopen (filename, "wb");
  if (!fp)
    exit (1);

  png = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png)
    exit (1);

  png_init_io (png, fp);

  pnginfo->valid = PNG_INFO_sBIT;
  pnginfo->sig_bit.red = 8;
  pnginfo->sig_bit.green = 8;
  pnginfo->sig_bit.blue = 8;

  png_write_info (png, pnginfo);
  png_write_image (png, rows);
  png_write_end (png, pngend);

  return 1;
}


/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include "gcmm.h"

typedef struct SigMean
{
  union
  {
    icSignature s;
    struct
    {
      char b0;
      char b1;
      char b2;
      char b3;
    }c;
  }u;
  char *meaning;
}SIGMEAN;

static SIGMEAN SigCMM[] =
{
  {{0x6170706cL}, "Apple (ColorSync�)"},
  {{0x4b434d53L}, "Microsoft (Kodak� Color Managing System)"},
  {{0x6e6f6e65L}, "none"},
  {{0x00000000L}, NULL}
};

static SIGMEAN SigPlatform[] =
{
  {{0x4150504CL}, "Apple Computer, Inc."},
  {{0x4D534654L}, "Microsoft Corporation"},
  {{0x53554E57L}, "Sun Microsystems, Inc."},
  {{0x53474920L}, "Silicon Graphics, Inc."},
  {{0x54474E54L}, "Taligent, Inc."},
  {{0x00000000L}, NULL}
};

static SIGMEAN SigManufacturer[] =
{
  {{0x6e6f6e65L}, "none"},
  {{0x4550534FL}, "Epson"},
  {{0x48502020L}, "Hewlett-Packard"},
  {{0x00000000L}, NULL}
};

static SIGMEAN SigModel[] =
{
  {{0x6e6f6e65L}, "none"},
  {{0x31333834L}, "Stylus Color 600"},
  {{0x46363734L}, "DeskJet Series"},
  {{0x00000000L}, NULL}
};

const char *
gcmmCMMinfo (icSignature sig)
{
  int i;

  for (i = 0; SigCMM[i].u.s; i++)
    if (sig == SigCMM[i].u.s)
      break;
  if (sig != SigCMM[i].u.s)
  {
    SIGMEAN new;
    new.u.s = sig;
#ifndef WORDS_BIGENDIAN
    fprintf (stderr, "Unknown CMM Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b3, new.u.c.b2,
	     new.u.c.b1, new.u.c.b0);
#else
    fprintf (stderr, "Unknown CMM Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b0, new.u.c.b1,
	     new.u.c.b2, new.u.c.b3);
#endif
    return NULL;
  }
  return SigCMM[i].meaning;
}

const char *
gcmmPlatformInfo (icSignature sig)
{
  int i;

  for (i = 0; SigPlatform[i].u.s; i++)
    if (sig == SigPlatform[i].u.s)
      break;
  if (sig != SigPlatform[i].u.s)
  {
    SIGMEAN new;
    new.u.s = sig;
#ifndef WORDS_BIGENDIAN
    fprintf (stderr, "Unknown Platform Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b3, new.u.c.b2,
	     new.u.c.b1, new.u.c.b0);
#else
    fprintf (stderr, "Unknown Platform Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b0, new.u.c.b1,
	     new.u.c.b2, new.u.c.b3);
#endif
    return NULL;
  }
  return SigPlatform[i].meaning;
}

const char *
gcmmManufacturerInfo (icSignature sig)
{
  int i;

  for (i = 0; SigManufacturer[i].u.s; i++)
    if (sig == SigManufacturer[i].u.s)
      break;
  if (sig != SigManufacturer[i].u.s)
  {
    SIGMEAN new;
    new.u.s = sig;
#ifndef WORDS_BIGENDIAN
    fprintf (stderr, "Unknown Manufacturer Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b3, new.u.c.b2,
	     new.u.c.b1, new.u.c.b0);
#else
    fprintf (stderr, "Unknown Manufacturer Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b0, new.u.c.b1,
	     new.u.c.b2, new.u.c.b3);
#endif
    return NULL;
  }
  return SigManufacturer[i].meaning;
}

const char *
gcmmModelInfo (icSignature sig)
{
  int i;

  for (i = 0; SigModel[i].u.s; i++)
    if (sig == SigModel[i].u.s)
      break;
  if (sig != SigModel[i].u.s)
  {
    SIGMEAN new;
    new.u.s = sig;
#ifndef WORDS_BIGENDIAN
    fprintf (stderr, "Unknown Model Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b3, new.u.c.b2,
	     new.u.c.b1, new.u.c.b0);
#else
    fprintf (stderr, "Unknown Model Signature 0x%x \"%c%c%c%c\". "
	     "Add to gcmmsignature.c\n", (int)sig, new.u.c.b0, new.u.c.b1,
	     new.u.c.b2, new.u.c.b3);
#endif
    return NULL;
  }
  return SigModel[i].meaning;
}

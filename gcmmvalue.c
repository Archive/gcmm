/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include "gcmm.h"
#include "gcmmsignature.h"
#include "gcmmutil.h"

#define TAGPTR(type)       ((type *)profile->tags.tag[index].data)
#define TAGSIG             TAGPTR (icGenericType)->base.sig

static const char *strtechnology (icSignature sig);
static int findlut (icProfile *profile, char intent, int pcs);
int gcmmisrequiredtag (icProfile *profile, icSignature sig);
int gcmmisprivatetag (icProfile *profile, icSignature sig);
static const char *colorspace (icColorSpaceSignature cs);

/*
 * header things
 */
int gcmmhisembedded (icProfile *profile);
int gcmmhisuseanywhere (icProfile *profile);
int gcmmhisreflective (icProfile *profile);
int gcmmhistransparency (icProfile *profile);
int gcmmhisglossy (icProfile *profile);
int gcmmhismatte (icProfile *profile);
const char *gcmmhgetclass (icProfile *profile);
const char *gcmmhgetdevicespace (icProfile *profile);
const char *gcmmhgetconnectionspace (icProfile *profile);
const char *gcmmhgetprofiledate (icProfile *profile);
const char *gcmmhgetplatform (icProfile *profile);
const char *gcmmhgetmanufacturer (icProfile *profile);
const char *gcmmhgetmodel (icProfile *profile);
const char *gcmmhgetcmm (icProfile *profile);
const char *gcmmhgetrenderingintent (icProfile *profile);
double *gcmmhgetilluminant (icProfile *profile);

/*
 * Tag list things
 */
unsigned long *gcmmgettags (icProfile *profile);
unsigned long *gcmmgetrequiredtags (icProfile *profile);
unsigned long *gcmmgetoptionaltags (icProfile *profile);

/*
 * things related to multi-dimensional lookup tables
 */
int gcmmgetluttype (icProfile *profile, const char *renderingintent, int to_pcs);
int gcmmgetlutinputchannels (icProfile *profile, const char *renderingintent,
			   int to_pcs);
int gcmmgetlutoutputchannels (icProfile *profile, const char *renderingintent,
			     int to_pcs);
int gcmmgetlutgridpoints (icProfile *profile, const char *renderingintent,
			int to_pcs);
int gcmmgetlutinputentries (icProfile *profile, const char *renderingintent,
			  int to_pcs);
int gcmmgetlutoutputentries (icProfile *profile, const char *renderingintent,
			   int to_pcs);
int gcmmgetclutsize (icProfile *profile, const char *renderingintent,
		   int to_pcs);
double *gcmmgetlutmatrix (icProfile *profile, const char *renderingintent,
			int to_pcs);
void *gcmmgetlutinput (icProfile *profile, const char *renderingintent,
		     int to_pcs, int channel);
void *gcmmgetlutoutput (icProfile *profile, const char *
				renderingintent, int to_pcs, int channel);
void *gcmmgetlutentry (icProfile *profile, const char *
				renderingintent, int to_pcs, ...);

/*
 * Thing related to matrix/curve based tables
 */

double *gcmmgetcolorant (icProfile *profile, int channel);
int gcmmgetTRCcount (icProfile *profile, int channel);
unsigned short gcmmgetTRC (icProfile *profile, int channel, int nth);

/*
 * misc things
 */

const char *gcmmgetcalibrationdate (icProfile *profile);
const char *gcmmgetcharacterization (icProfile *profile);
const char *gcmmgetcopyright (icProfile *profile);
const char *gcmmgetmanufacturer (icProfile *profile);
const char *gcmmgetmodel (icProfile *profile);
double gcmmgetluminance (icProfile *profile);
const char *gcmmgetstdobserver (icProfile *profile);
double *gcmmgetbacking (icProfile *profile);
const char *gcmmgetgeometry (icProfile *profile);
const char *gcmmgetflare (icProfile *profile);
const char *gcmmgetilluminant (icProfile *profile);
double *gcmmgetblackpoint (icProfile *profile);
double *gcmmgetwhitepoint (icProfile *profile);
int gcmmgetcountnames (icProfile *profile);
const char *gcmmgetcolorprefix (icProfile *profile);
const char *gcmmgetcolorsuffix (icProfile *profile);
const char *gcmmgetcolorname (icProfile *profile, int nth);
double *gcmmgetcolorpcs (icProfile *profile, int nth);
double *gcmmgetcolordvc (icProfile *profile, int nth);
const char *gcmmgetprofiledescription (icProfile *profile);
int gcmmgetsequencecount (icProfile *profile);
const char *gcmmgetsequence (icProfile *profile, int nth);
const char *gcmmgettechnology (icProfile *profile);
int gcmmgetucrcount (icProfile *profile);
unsigned short int *gcmmgetucr (icProfile *profile);
int gcmmgetbgcount (icProfile *profile);
unsigned short int *gcmmgetbg (icProfile *profile);
const char *gcmmgetscreeningdesc (icProfile *profile);
int gcmmisprinterdefaultscreen (icProfile *profile);
int gcmmsetprinterdefaultscreen (icProfile *profile, int val);
int gcmmislinesperinch (icProfile *profile);
int gcmmscreenchannels (icProfile *profile);
double gcmmscreenfrequency (icProfile *profile, int channel);
double gcmmscreenangle (icProfile *profile, int channel);
const char *gcmmscreenshape (icProfile *profile, int channel);
const char *gcmmgetviewingdesc (icProfile *profile);
double *gcmmgetviewingilluminant (icProfile *profile);
double *gcmmgetviewingsurround (icProfile *profile);
const char *gcmmgetviewingstdilluminant (icProfile *profile);
const char *gcmmpsname (icProfile *profile);
const char *gcmmpscrdname (icProfile *profile, char *renderingintent);
int gcmmpscrdcount (icProfile *profile, char *renderingintent);
int gcmmisbinpscrd (icProfile *profile, char *renderingintent);
unsigned char *gcmmgetpscrd (icProfile *profile, char *renderingintent);
unsigned char *gcmmgetpscsa (icProfile *profile);
unsigned char *gcmmgetpsrenderingintent (icProfile *profile);

/*
 * Changing the world
 */

int gcmmhsetrenderingintent (icProfile *profile, const char *intent);
int gcmmsetlutmatrix (icProfile *profile, const char *renderingintent,
		    int to_pcs, double matrix[9]);
int gcmmsetlutinput (icProfile *profile, const char *renderingintent, int to_pcs,
		   int channel, unsigned short *array);
int gcmmsetlutoutput (icProfile *profile, const char *renderingintent,
		    int to_pcs, int channel, unsigned short *array);
int gcmmsetlutentry (icProfile *profile, const char *renderingintent, int to_pcs,
		   unsigned short *newarray, ...);
int gcmmsetcolorant (icProfile *profile, double *col, int channel);
int gcmmsetTRC (icProfile *profile, int to_pcs, int channel, unsigned short val,
	      int nth);

/*******************************************************************
 *                           EXECUTABLES                           *
 *******************************************************************/


/*
 * Header related things
 */

const char *
gcmmhgetclass (icProfile *profile)
{
  switch (profile->header.deviceClass)
  {
    case icSigInputClass:
      return "input";
    case icSigDisplayClass:
      return "display";
    case icSigOutputClass:
      return "output";
    case icSigLinkClass:
      return "link";
    case icSigAbstractClass:
      return "abstract";
    case icSigColorSpaceClass:
      return "color space conversion";
    case icSigNamedColorClass:
      return "named color";
    default:
      return "unknown";
  }
}

static const char *
colorspace (icColorSpaceSignature cs)
{
  switch (cs)
  {
    case icSigXYZData:
      return "XYZ";
    case icSigLabData:
      return "Lab";
    case icSigLuvData:
      return "Luv";
    case icSigYCbCrData:
      return "YCbCr";
    case icSigYxyData:
      return "Yxy";
    case icSigRgbData:
      return "RGB";
    case icSigGrayData:
      return "gray";
    case icSigHsvData:
      return "HSV";
    case icSigHlsData:
      return "HLS";
    case icSigCmykData:
      return "CMYK";
    case icSigCmyData:
      return "CMY";
    case icSig2colorData:
      return "2 colors";
    case icSig3colorData:
      return "3 colors";
    case icSig4colorData:
      return "4 colors";
    case icSig5colorData:
      return "5 colors";
    case icSig6colorData:
      return "6 colors";
    case icSig7colorData:
      return "7 colors";
    case icSig8colorData:
      return "8 colors";
    case icSig9colorData:
      return "9 colors";
    case icSig10colorData:
      return "10 colors";
    case icSig11colorData:
      return "11 colors";
    case icSig12colorData:
      return "12 colors";
    case icSig13colorData:
      return "13 colors";
    case icSig14colorData:
      return "14 colors";
    case icSig15colorData:
      return "15 colors";
    default:
      return "unknown";
  }
}

const char *
gcmmhgetdevicespace (icProfile *profile)
{
  return colorspace (profile->header.colorSpace);
}

const char *
gcmmhgetconnectionspace (icProfile *profile)
{
  return colorspace (profile->header.pcs);
}

const char *
gcmmhgetprofiledate (icProfile *profile)
{
  return gcmmstricdate (&profile->header.date);
}

const char *
gcmmhgetplatform (icProfile *profile)
{
  const char *ptr = gcmmPlatformInfo (profile->header.platform);

  if (!ptr)
    return "unknown";
  else
    return ptr;
}

int
gcmmhisembedded (icProfile *profile)
{
  return profile->header.flags & icEmbeddedProfileTrue;
}

int
gcmmhisuseanywhere (icProfile *profile)
{
  return (profile->header.flags & icUseWithEmbeddedDataOnly) ?
    false : true;
}

const char *
gcmmhgetmanufacturer (icProfile *profile)
{
  const char *ptr = gcmmManufacturerInfo (profile->header.manufacturer);

  if (!ptr)
    return "unknown";
  else
    return ptr;
}

const char *
gcmmhgetmodel (icProfile *profile)
{
  const char *ptr = gcmmModelInfo (profile->header.model);

  if (!ptr)
    return "unknown";
  else
    return ptr;
}

const char *
gcmmhgetcmm (icProfile *profile)
{
  const char *ptr = gcmmCMMinfo (profile->header.cmmId);

  if (!ptr)
    return "unknown";
  else
    return ptr;
}

int
gcmmhisreflective (icProfile *profile)
{
  return (profile->header.attributes & icTransparency) ? false : true;
}

int
gcmmhistransparency (icProfile *profile)
{
  return profile->header.attributes & icTransparency;
}

int
gcmmhisglossy (icProfile *profile)
{
  return (profile->header.attributes & icMatte) ? false : true;
}

int
gcmmhismatte (icProfile *profile)
{
  return profile->header.attributes & icMatte;
}

const char *
gcmmhgetrenderingintent (icProfile *profile)
{
  switch (profile->header.renderingIntent)
  {
    case icPerceptual:
      return "perceptual";
    case icRelativeColorimetric:
      return "relative colorimetric";
    case icSaturation:
      return "saturation";
    case icAbsoluteColorimetric:
      return "absolute colorimetric";
    default:
      return "unknown";
  }
}

int
gcmmhsetrenderingintent (icProfile *profile, const char *intent)
{
  switch (*intent)
  {
    case 'p':
      profile->header.renderingIntent = icPerceptual;
      break;
    case 'r':
      profile->header.renderingIntent = icRelativeColorimetric;
      break;
    case 's':
      profile->header.renderingIntent = icSaturation;
      break;
    case 'a':
      profile->header.renderingIntent = icAbsoluteColorimetric;
      break;
    default:
      return false;
  }
  return true;
}

double *
gcmmhgetilluminant (icProfile *profile)
{
  static double il[3];

  il[0] = gcmmds15f16 (profile->header.illuminant.X);
  il[1] = gcmmds15f16 (profile->header.illuminant.Y);
  il[2] = gcmmds15f16 (profile->header.illuminant.Z);

  return il;
}

/*
 * Tagged information
 */

int
gcmmisrequiredtag (icProfile *profile, icSignature sig)
{
  if (sig == icSigProfileDescriptionTag)
    return true;

  if (sig == icSigCopyrightTag)
    return true;

  if (profile->header.deviceClass != icSigLinkClass
      && !gcmmfindtag (profile, icSigMediaWhitePointTag))
    return true;

  if (profile->header.deviceClass == icSigInputClass
      || profile->header.deviceClass == icSigDisplayClass)
  {
    if ((gcmmncomponents (profile->header.colorSpace) == 1)
	&& sig == icSigGrayTRCTag)
      return true;
    else if (profile->header.pcs == icSigLabData && sig == icSigAToB0Tag)
      return true;
    else if (!gcmmfindtag (profile, icSigAToB0Tag))
    {
      if (sig == icSigRedColorantTag)
	return true;
      if (sig == icSigGreenColorantTag)
	return true;
      if (sig == icSigBlueColorantTag)
	return true;
      if (sig == icSigRedTRCTag)
	return true;
      if (sig == icSigGreenTRCTag)
	return true;
      if (sig == icSigBlueTRCTag)
	return true;
    }
  }

  if (profile->header.deviceClass == icSigOutputClass)
  {
    if (gcmmncomponents (profile->header.colorSpace) == 1
	&& sig == icSigGrayTRCTag)
      return true;
    else
    {
      if (sig == icSigAToB0Tag)
	return true;
      if (sig == icSigAToB1Tag)
	return true;
      if (sig == icSigAToB2Tag)
	return true;
      if (sig == icSigBToA0Tag)
	return true;
      if (sig == icSigBToA1Tag)
	return true;
      if (sig == icSigBToA0Tag)
	return true;
      if (sig == icSigGamutTag)
	return true;
    }
  }

  if ((profile->header.deviceClass == icSigLinkClass
       || profile->header.deviceClass == icSigAbstractClass
       || profile->header.deviceClass == icSigColorSpaceClass)
      && sig == icSigAToB0Tag)
    return true;

  if (profile->header.deviceClass == icSigLinkClass
      && sig == icSigProfileSequenceDescTag)
    return true;

  if (profile->header.deviceClass == icSigColorSpaceClass
      && sig == icSigBToA0Tag)
    return true;

  if (profile->header.deviceClass == icSigNamedColorClass
      && sig == icSigNamedColor2Tag)
    return true;

  return false;
}

int
gcmmisprivatetag (icProfile *profile, icSignature sig)
{
  switch (sig)
  {
    case icSigAToB0Tag:
    case icSigAToB1Tag:
    case icSigAToB2Tag:
    case icSigBlueColorantTag:
    case icSigBlueTRCTag:
    case icSigBToA0Tag:
    case icSigBToA1Tag:
    case icSigBToA2Tag:
    case icSigCalibrationDateTimeTag:
    case icSigCharTargetTag:
    case icSigCopyrightTag:
    case icSigDeviceMfgDescTag:
    case icSigDeviceModelDescTag:
    case icSigGamutTag:
    case icSigGrayTRCTag:
    case icSigGreenColorantTag:
    case icSigGreenTRCTag:
    case icSigLuminanceTag:
    case icSigMeasurementTag:
    case icSigMediaBlackPointTag:
    case icSigMediaWhitePointTag:
    case icSigNamedColorTag:
    case icSigPreview0Tag:
    case icSigPreview1Tag:
    case icSigPreview2Tag:
    case icSigProfileDescriptionTag:
    case icSigProfileSequenceDescTag:
    case icSigPs2CRD0Tag:
    case icSigPs2CRD1Tag:
    case icSigPs2CRD2Tag:
    case icSigPs2CRD3Tag:
    case icSigPs2CSATag:
    case icSigPs2RenderingIntentTag:
    case icSigRedColorantTag:
    case icSigRedTRCTag:
    case icSigScreeningDescTag:
    case icSigScreeningTag:
    case icSigTechnologyTag:
    case icSigUcrBgTag:
    case icSigViewingCondDescTag:
    case icSigViewingConditionsTag:
    case icSigNamedColor2Tag:
    case icSigCrdInfoTag:
      return false;
    default:
      return true;
  }
}

unsigned long *
gcmmgettags (icProfile *profile)
{
  static unsigned long tags[64];
  int i;

  for (i = 0; i < 64; i++)
    if (i < profile->tags.count)
      tags[i] = (unsigned long)profile->tags.tag[i].sig;
    else
      tags[i] = 0L;

  return tags;
}

unsigned long *
gcmmgetrequiredtags (icProfile *profile)
{
  static unsigned long tags[64];
  int index, i;

  for (index = 0, i = 0; index < 64; index++, i++)
  {
    if (index < profile->tags.count)
    {
      if (!gcmmisprivatetag (profile, profile->tags.tag[index].sig)
	  && gcmmisrequiredtag (profile, profile->tags.tag[index].sig))
	tags[i] = profile->tags.tag[index].sig;
      else
	i--;
    }
    else
      tags[i] = 0L;
  }

  return tags;
}

unsigned long *
gcmmgetoptionaltags (icProfile *profile)
{
  static unsigned long tags[64];
  int index, i;

  for (index = 0, i = 0; index < 64; index++, i++)
  {
    if (index < profile->tags.count)
    {
      if (!gcmmisprivatetag (profile, profile->tags.tag[index].sig)
	  && !gcmmisrequiredtag (profile, profile->tags.tag[index].sig))
	tags[i] = profile->tags.tag[index].sig;
      else
	i--;
    }
    else
      tags[i] = 0L;
  }

  return tags;
}

/*
 * routines related to multi-dimensional lookup tables
 */

static int
findlut (icProfile *profile, char intent, int pcs)
{
  icSignature sig;
  int preview = 0;

  if (intent >= 'A' && intent <= 'Z')
  {
    preview++;
    intent += 'A';
  }

  if (intent == 'g')
    sig = icSigGamutTag;
  else
  {
    if (pcs)
    {
      switch (intent)
      {
        case 'p':
	  sig = preview ? icSigPreview0Tag : icSigAToB0Tag;
	  break;
        case 'r':
        case 'a':
	  sig = preview ? icSigPreview1Tag : icSigAToB1Tag;
	  break;
        case 's':
	  sig = preview ? icSigPreview2Tag : icSigAToB2Tag;
	  break;
        default:
	  return -1;
      }
    }
    else
    {
      switch (intent)
      {
        case 'p':
	  sig = preview ? icSigPreview0Tag : icSigBToA0Tag;
	  break;
        case 'r':
        case 'a':
	  sig = preview ? icSigPreview1Tag : icSigBToA1Tag;
	  break;
        case 's':
	  sig = preview ? icSigPreview2Tag : icSigBToA2Tag;
	  break;
        default:
	  return -1;
      }
    }
  }

  return gcmmfindtag (profile, sig);
}

int
gcmmgetluttype (icProfile *profile, const char *renderingintent, int to_pcs)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
    return 256;
  else if (TAGSIG == icSigLut16Type)
    return  65536;
  else
    return false;
}
/*
 * possible values for rendering intent:
 *         "perceptual",
 *         "relative colorimetric",
 *         "saturation",
 *         "absolute colorimetric", and
 *         "gamut".
 * (first letter enough). gamut is not a rendering intent, but serves to
 * select a LUT. in this case, 'to_pcs' is not observed. If (at least) the
 * first letter is uppercase, a preview tag will be used.
 */

int
gcmmgetlutinputchannels (icProfile *profile, const char *renderingintent,
		       int to_pcs)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
    return TAGPTR (icLut8Type)->lut.inputChan;
  else if (TAGSIG == icSigLut16Type)
    return  TAGPTR (icLut16Type)->lut.inputChan;
  else
    return false;
}

int
gcmmgetlutoutputchannels (icProfile *profile, const char *renderingintent,
			 int to_pcs)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
    return TAGPTR (icLut8Type)->lut.outputChan;
  else if (TAGSIG == icSigLut16Type)
    return TAGPTR (icLut16Type)->lut.outputChan;
  else
    return false;
}

int
gcmmgetlutgridpoints (icProfile *profile, const char *renderingintent,
		    int to_pcs)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
    return TAGPTR (icLut8Type)->lut.clutPoints;
  else if (TAGSIG == icSigLut16Type)
    return TAGPTR (icLut16Type)->lut.clutPoints;
  else
    return false;
}

int
gcmmgetlutinputentries (icProfile *profile, const char *renderingintent,
		      int to_pcs)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
    return 256;
  else if (TAGSIG == icSigLut16Type)
    return TAGPTR (icLut16Type)->lut.inputEnt;
  else
    return false;
}

int
gcmmgetlutoutputentries (icProfile *profile, const char *renderingintent,
		      int to_pcs)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
    return 256;
  else if (TAGSIG == icSigLut16Type)
    return TAGPTR (icLut16Type)->lut.outputEnt;
  else
    return false;
}

double *
gcmmgetlutmatrix (icProfile *profile, const char *renderingintent, int to_pcs)
{
  static double matrix[9];
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return NULL;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);

    matrix[0] = gcmmds15f16 (lut->e00);
    matrix[1] = gcmmds15f16 (lut->e01);
    matrix[2] = gcmmds15f16 (lut->e02);
    matrix[3] = gcmmds15f16 (lut->e10);
    matrix[4] = gcmmds15f16 (lut->e11);
    matrix[5] = gcmmds15f16 (lut->e12);
    matrix[6] = gcmmds15f16 (lut->e20);
    matrix[7] = gcmmds15f16 (lut->e21);
    matrix[8] = gcmmds15f16 (lut->e22);
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR(icLut16Type)->lut);

    matrix[0] = gcmmds15f16 (lut->e00);
    matrix[1] = gcmmds15f16 (lut->e01);
    matrix[2] = gcmmds15f16 (lut->e02);
    matrix[3] = gcmmds15f16 (lut->e10);
    matrix[4] = gcmmds15f16 (lut->e11);
    matrix[5] = gcmmds15f16 (lut->e12);
    matrix[6] = gcmmds15f16 (lut->e20);
    matrix[7] = gcmmds15f16 (lut->e21);
    matrix[8] = gcmmds15f16 (lut->e22);
  }
  else
    return NULL;

  return matrix;
}

int
gcmmsetlutmatrix (icProfile *profile, const char *renderingintent, int to_pcs,
		double matrix[9])
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR(icLut8Type)->lut);

    lut->e00 = gcmms15f16d (matrix[0]);
    lut->e01 = gcmms15f16d (matrix[1]);
    lut->e02 = gcmms15f16d (matrix[2]);
    lut->e10 = gcmms15f16d (matrix[3]);
    lut->e11 = gcmms15f16d (matrix[4]);
    lut->e12 = gcmms15f16d (matrix[5]);
    lut->e20 = gcmms15f16d (matrix[6]);
    lut->e21 = gcmms15f16d (matrix[7]);
    lut->e22 = gcmms15f16d (matrix[8]);
  }
  else if (TAGSIG  == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);

    lut->e00 = gcmms15f16d (matrix[0]);
    lut->e01 = gcmms15f16d (matrix[1]);
    lut->e02 = gcmms15f16d (matrix[2]);
    lut->e10 = gcmms15f16d (matrix[3]);
    lut->e11 = gcmms15f16d (matrix[4]);
    lut->e12 = gcmms15f16d (matrix[5]);
    lut->e20 = gcmms15f16d (matrix[6]);
    lut->e21 = gcmms15f16d (matrix[7]);
    lut->e22 = gcmms15f16d (matrix[8]);
  }
  else
    return false;

  return true;
}

void *
gcmmgetlutinput (icProfile *profile, const char *renderingintent, int to_pcs,
	       int channel)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return NULL;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);

    if (channel > lut->inputChan - 1)
      return NULL;

    return lut->inputTable + (channel * 256);
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);

    if (channel > lut->inputChan - 1)
      return NULL;

    return lut->inputTable + (channel * lut->inputEnt);
  }

  return NULL;
}

int
gcmmsetlutinput (icProfile *profile, const char *renderingintent, int to_pcs,
	       int channel, unsigned short *array)
{
  unsigned short *ptr;
  unsigned char *ptr1;
  int index, i;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);

    if (channel > lut->inputChan - 1)
      return false;

    ptr1 = lut->inputTable + (channel * 256);

    for (i = 0; i < 256; i++)
      ptr1[i] = (unsigned char)(array[i]);
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);

    if (channel > lut->inputChan - 1)
      return false;

    ptr = lut->inputTable + (channel * lut->inputEnt);

    for (i = 0; i < lut->inputEnt; i++)
      ptr[i] = array[i];
  }
  else
    return false;

  return true;
}

void *
gcmmgetlutoutput (icProfile *profile, const char *renderingintent, int to_pcs,
	       int channel)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return NULL;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);

    if (channel > lut->outputChan - 1)
      return NULL;

    return lut->outputTable + (channel * 256);
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);

    if (channel > lut->outputChan - 1)
      return NULL;

    return lut->outputTable + (channel * lut->outputEnt);
  }

  return NULL;
}

int
gcmmsetlutoutput (icProfile *profile, const char *renderingintent, int to_pcs,
	       int channel, unsigned short *array)
{
  unsigned short *ptr;
  unsigned char *ptr1;
  int index, i;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);

    if (channel > lut->outputChan - 1)
      return false;

    ptr1 = lut->outputTable + (channel * 256);

    for (i = 0; i < 256; i++)
      ptr1[i] = (unsigned char)(array[i]);
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);

    if (channel > lut->outputChan - 1)
      return false;

    ptr = lut->outputTable + (channel * lut->outputEnt);

    for (i = 0; i < lut->outputEnt; i++)
      ptr[i] = array[i];
  }
  else
    return false;

  return true;
}

int
gcmmgetclutsize (icProfile *profile, const char *renderingintent, int to_pcs)
{
  int index;

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);

    return pow (lut->clutPoints, lut->inputChan) * lut->outputChan;
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);

    return pow (lut->clutPoints, lut->inputChan) * lut->outputChan;
  }
  else
    return false;
}


/*
 * gcmmgetlutentry: usage
 *          ptr = gcmmgetlutentry (prof, "p", 1, red, gree, blue)
 *          cyan    = ptr[0];
 *          magenta = ptr[1];
 *          yellow  = ptr[2];
 *          black   = ptr[3];
 */

void *
gcmmgetlutentry (icProfile *profile, const char *renderingintent, int to_pcs,
	       ...)
{
  static unsigned short array[MAX_CHAN];
  int index, i, ncols;
  va_list ap;

  if (to_pcs)
    ncols = gcmmncomponents (profile->header.colorSpace);
  else
    ncols = gcmmncomponents (profile->header.pcs);

  va_start (ap, to_pcs);
  for (i=0; i<ncols; i++)
    array[i] = va_arg (ap, short);
  va_end (ap);

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return NULL;

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);
    int offset = 0;

    for (i = lut->inputChan - 1; i >= 0; i--)
    {
      int chan = lut->inputChan - i - 1;
      offset += pow (lut->clutPoints, i) * array[chan];
    }
    offset *= lut->outputChan;
    return lut->clutTable + offset;
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);
    int offset = 0;

    for (i = lut->inputChan - 1; i >= 0; i--)
    {
      int chan = lut->inputChan - i - 1;
      offset += pow (lut->clutPoints, i) * array[chan];
    }
    offset *= lut->outputChan;
    return lut->clutTable + offset;
  }

  return NULL;
}

/*
 * gcmmsetlutentry: usage
 *          ptr[0] = new_cyan;
 *          ptr[1] = new_magenta;
 *          ptr[2] = new_yellow;
 *          ptr[3] = new_black;
 *          success = gcmmgetlutentry (prof, "p", 1, ptr, red, gree, blue)
 */

int
gcmmsetlutentry (icProfile *profile, const char *renderingintent, int to_pcs,
	       unsigned short *newarray, ...)
{
  unsigned short array[MAX_CHAN], *ptr;
  unsigned char *ptr1;
  int index, i, size, ncols;
  va_list ap;

  if (to_pcs)
    ncols = gcmmncomponents (profile->header.colorSpace);
  else
    ncols = gcmmncomponents (profile->header.pcs);

  va_start (ap, newarray);
  for (i=0; i<ncols; i++)
    array[i] = va_arg (ap, short);
  va_end (ap);

  index = findlut (profile, *renderingintent, to_pcs);
  if (index == -1)
    return false;

  size = gcmmgetclutsize (profile, renderingintent, to_pcs);

  if (TAGSIG == icSigLut8Type)
  {
    icLut8 *lut = &(TAGPTR (icLut8Type)->lut);
    int offset = 0;

    for (i = lut->inputChan - 1; i >= 0; i--)
    {
      int chan = lut->inputChan - i - 1;
      offset += pow (lut->clutPoints, i) * array[chan];
    }
    offset *= lut->outputChan;
    ptr1 = lut->clutTable + offset;

    for (i = 0; i < lut->outputChan; i++)
      ptr1[i] = (unsigned char)(newarray[i]);
  }
  else if (TAGSIG == icSigLut16Type)
  {
    icLut16 *lut = &(TAGPTR (icLut16Type)->lut);
    int offset = 0;

    for (i = lut->inputChan - 1; i >= 0; i--)
    {
      int chan = lut->inputChan - i - 1;
      offset += pow (lut->clutPoints, i) * array[chan];
    }
    offset *= lut->outputChan;
    ptr = lut->clutTable + offset;

    for (i = 0; i < lut->outputChan; i++)
      ptr[i] = newarray[i];
  }
  else
    return false;

  return true;
}

/*
 * Matrix based conversion info.
 */

double *
gcmmgetcolorant (icProfile *profile, int channel)
{
  static double col[3];
  icSignature sig;
  icXYZType *xyz;
  int index;

  switch (channel)
  {
    case 0:
      sig = icSigRedColorantTag;
      break;
    case 1:
      sig = icSigGreenColorantTag;
      break;
    case 2:
      sig = icSigBlueColorantTag;
      break;
    default:
      return NULL;
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return NULL;

  xyz = TAGPTR (icXYZType);

  col[0] = gcmmds15f16 (xyz->data->X);
  col[1] = gcmmds15f16 (xyz->data->Y);
  col[2] = gcmmds15f16 (xyz->data->Z);

  return col;
}

int
gcmmsetcolorant (icProfile *profile, double *col, int channel)
{
  icSignature sig;
  icXYZType *xyz;
  int index;

  switch (channel)
  {
    case 0:
      sig = icSigRedColorantTag;
      break;
    case 1:
      sig = icSigGreenColorantTag;
      break;
    case 2:
      sig = icSigBlueColorantTag;
      break;
    default:
      return false;
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return false;

  xyz =TAGPTR (icXYZType);

  xyz->data->X = gcmms15f16d (col[0]);
  xyz->data->Y = gcmms15f16d (col[1]);
  xyz->data->Z = gcmms15f16d (col[1]);

  return true;
}

/*
 * NOTE: if colorSpace is gray, channel is not observed
 */

int
gcmmgetTRCcount (icProfile *profile, int channel)
{
  icSignature sig;
  int index;

  if (gcmmncomponents (profile->header.colorSpace) == 1)
    sig = icSigGrayTRCTag;
  else
  {
    switch (channel)
    {
      case 0:
	sig = icSigRedTRCTag;
	break;
      case 1:
	sig = icSigGreenTRCTag;
	break;
      case 2:
	sig = icSigBlueTRCTag;
	break;
      default:
	return false;
    }
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return false;

  return TAGPTR (icCurveType)->curve.count;
}

unsigned short
gcmmgetTRC (icProfile *profile, int channel, int nth)
{
  icSignature sig;
  int index;

  if (gcmmncomponents (profile->header.colorSpace) == 1)
    sig = icSigGrayTRCTag;
  else
  {
    switch (channel)
    {
      case 0:
	sig = icSigRedTRCTag;
	break;
      case 1:
	sig = icSigGreenTRCTag;
	break;
      case 2:
	sig = icSigBlueTRCTag;
	break;
      default:
	return false;
    }
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return false;

  return TAGPTR (icCurveType)->curve.data[nth];
}

int
gcmmsetTRC (icProfile *profile, int to_pcs, int channel, unsigned short val,
	  int nth)
{
  icSignature sig;
  int index;

  if (gcmmncomponents (profile->header.colorSpace) == 1)
    sig = icSigGrayTRCTag;
  else
  {
    switch (channel)
    {
      case 0:
	sig = icSigRedTRCTag;
	break;
      case 1:
	sig = icSigGreenTRCTag;
	break;
      case 2:
	sig = icSigBlueTRCTag;
	break;
      default:
	return false;
    }
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return false;

  TAGPTR (icCurveType)->curve.data[nth] = val;

  return true;
}

/*
 * other info
 */

const char *
gcmmgetcalibrationdate (icProfile *profile)
{
  int index = gcmmfindtag(profile, icSigCalibrationDateTimeTag);

  if (index == -1)
    return NULL;

  return gcmmstricdate (&(TAGPTR (icDateTimeType)->date));
}

const char *
gcmmgetcharacterization (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigCharTargetTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icTextType)->data;
}

const char *
gcmmgetcopyright (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigCopyrightTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icTextType)->data;
}

const char *
gcmmgetmanufacturer (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigDeviceMfgDescTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icTextDescriptionType)->desc.desc;
}

const char *
gcmmgetmodel (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigDeviceModelDescTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icTextDescriptionType)->desc.desc;
}

double
gcmmgetluminance (icProfile *profile)
{
  icXYZType *xyz;
  int index;

  index = gcmmfindtag (profile, icSigLuminanceTag);
  if (index == -1)
    return -1.0;

  xyz = TAGPTR (icXYZType);

  return gcmmds15f16 (xyz->data->Y);
}

const char *
gcmmgetstdobserver (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigMeasurementTag);

  if (index == -1)
    return NULL;

  switch (TAGPTR (icMeasurementType)->measurement.stdObserver)
  {
    case icStdObs1931TwoDegrees:
      return "2 degrees";
    case icStdObs1964TenDegrees:
      return "10 degrees";
    default:
      return "unknown";
  }
}

double *
gcmmgetbacking (icProfile *profile)
{
  static double res[3];
  icXYZNumber *xyz;
  int index;

  index = gcmmfindtag (profile, icSigMeasurementTag);
  if (index == -1)
    return NULL;

  xyz = &(TAGPTR (icMeasurementType)->measurement.backing);

  res[0] = gcmmds15f16 (xyz->X);
  res[1] = gcmmds15f16 (xyz->Y);
  res[2] = gcmmds15f16 (xyz->Z);

  return res;
}

const char *
gcmmgetgeometry (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigMeasurementTag);

  if (index == -1)
    return NULL;

  switch (TAGPTR (icMeasurementType)->measurement.geometry)
  {
    case icGeometry045or450:
      return "0/45 or 45/0";
    case icGeometry0dord0:
      return "0D or D0";
    default:
      return "unknown";
  }
}

const char *
gcmmgetflare (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigMeasurementTag);

  if (index == -1)
    return NULL;

  switch (TAGPTR (icMeasurementType)->measurement.flare)
  {
    case icFlare0:
      return "0 percent flare";
    case icFlare100:
      return "100 percent flare";
    default:
      return "unknown";
  }
}

const char *
gcmmgetilluminant (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigMeasurementTag);

  if (index == -1)
    return NULL;

  switch (TAGPTR (icMeasurementType)->measurement.illuminant)
  {
    case icIlluminantD50:
      return "D50";
    case icIlluminantD65:
      return "D65";
    case icIlluminantD93:
      return "D93";
    case icIlluminantF2:
      return "F2";
    case icIlluminantD55:
      return "D55";
    case icIlluminantA:
      return "A";
    case icIlluminantEquiPowerE:
      return "E";
    case icIlluminantF8:
      return "F8";
    default:
      return "unknown";
  }
}

double *
gcmmgetblackpoint (icProfile *profile)
{
  static double res[3];
  icXYZNumber *xyz;
  int index;

  index = gcmmfindtag (profile, icSigMediaBlackPointTag);
  if (index == -1)
    res[0] = res[1] = res[2] = 0.0;
  else
  {
    xyz = TAGPTR (icXYZType)->data;

    res[0] = gcmmds15f16 (xyz->X);
    res[1] = gcmmds15f16 (xyz->Y);
    res[2] = gcmmds15f16 (xyz->Z);
  }
  return res;
}

double *
gcmmgetwhitepoint (icProfile *profile)
{
  static double res[3];
  icXYZNumber *xyz;
  int index;

  index = gcmmfindtag (profile, icSigMediaWhitePointTag);
  if (index == -1)
    return NULL;

  xyz = TAGPTR (icXYZType)->data;

  res[0] = gcmmds15f16 (xyz->X);
  res[1] = gcmmds15f16 (xyz->Y);
  res[2] = gcmmds15f16 (xyz->Z);

  return res;
}

int
gcmmgetcountnames (icProfile *profile)
{
  int index, old = 0;

  index = gcmmfindtag (profile, icSigNamedColor2Tag);
  if (index == -1)
  {
    index = gcmmfindtag (profile, icSigNamedColorTag);
    if (index == -1)
      return 0;
    old++;
  }

  if (old)
    return TAGPTR (icNamedColorType)->ncolor.count;
  else
    return TAGPTR (icNamedColor2Type)->ncolor.count;
}

const char *
gcmmgetcolorprefix (icProfile *profile)
{
  int index, old = 0;

  index = gcmmfindtag (profile, icSigNamedColor2Tag);
  if (index == -1)
  {
    index = gcmmfindtag (profile, icSigNamedColorTag);
    if (index == -1)
      return 0;
    old++;
  }

  if (old)
    return TAGPTR (icNamedColorType)->ncolor.prefix;
  else
    return TAGPTR (icNamedColor2Type)->ncolor.prefix;
}

const char *
gcmmgetcolorsuffix (icProfile *profile)
{
  int index, old = 0;

  index = gcmmfindtag (profile, icSigNamedColor2Tag);
  if (index == -1)
  {
    index = gcmmfindtag (profile, icSigNamedColorTag);
    if (index == -1)
      return 0;
    old++;
  }

  if (old)
    return TAGPTR (icNamedColorType)->ncolor.suffix;
  else
    return TAGPTR (icNamedColor2Type)->ncolor.suffix;
}

const char *
gcmmgetcolorname (icProfile *profile, int nth)
{
  int index, old = 0;

  index = gcmmfindtag (profile, icSigNamedColor2Tag);
  if (index == -1)
  {
    index = gcmmfindtag (profile, icSigNamedColorTag);
    if (index == -1)
      return 0;
    old++;
  }

  if (old)
    return TAGPTR (icNamedColorType)->ncolor.name[nth].root;
  else
    return TAGPTR (icNamedColor2Type)->ncolor.name[nth].root;
}

double *
gcmmgetcolorpcs (icProfile *profile, int nth)
{
  static double col[MAX_CHAN];
  int index, old = 0, i;
  unsigned short *array;

  index = gcmmfindtag (profile, icSigNamedColor2Tag);
  if (index == -1)
  {
    index = gcmmfindtag (profile, icSigNamedColorTag);
    if (index == -1)
      return 0;
    old++;
  }

  if (old)
    array = TAGPTR (icNamedColorType)->ncolor.name[nth].pcsCoords;
  else
    array = TAGPTR (icNamedColor2Type)->ncolor.name[nth].pcsCoords;

  for (i=0; i<16; i++)
    col[i] = array[i];

  return col;
}

double *
gcmmgetcolordvc (icProfile *profile, int nth)
{
  static double col[MAX_CHAN];
  int index, old = 0, i;
  unsigned short *array;

  index = gcmmfindtag (profile, icSigNamedColor2Tag);
  if (index == -1)
  {
    index = gcmmfindtag (profile, icSigNamedColorTag);
    if (index == -1)
      return 0;
    old++;
  }

  if (old)
    array = TAGPTR (icNamedColorType)->ncolor.name[nth].deviceCoords;
  else
    array = TAGPTR (icNamedColor2Type)->ncolor.name[nth].deviceCoords;

  for (i=0; i<16; i++)
    col[i] = array[i];

  return col;
}

const char *
gcmmgetprofiledescription (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigProfileDescriptionTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icTextDescriptionType)->desc.desc;
}

int
gcmmgetsequencecount (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigProfileSequenceDescTag);

  if (index == -1)
    return false;

  return TAGPTR (icProfileSequenceDescType)->desc.count;
}

static const char *
strtechnology (icSignature sig)
{
  switch (sig)
  {
    case icSigDigitalCamera:
      return "Digital Camera";
    case icSigFilmScanner:
      return "Film Scanner";
    case icSigReflectiveScanner:
      return "Reflective Scanner";
    case icSigInkJetPrinter:
      return "Ink Jet Printer";
    case icSigThermalWaxPrinter:
      return "Thermal Wax Printer";
    case icSigElectrophotographicPrinter:
      return "Electrophotographic Printer";
    case icSigElectrostaticPrinter:
      return "Electrostatic Printer";
    case icSigDyeSublimationPrinter:
      return "Dye Sublimation Printer";
    case icSigPhotographicPaperPrinter:
      return "Photographic Paper Printer";
    case icSigFilmWriter:
      return "Film Writer";
    case icSigVideoMonitor:
      return "Video Monitor";
    case icSigVideoCamera:
      return "Video Camera";
    case icSigProjectionTelevision:
      return "Projection Television";
    case icSigCRTDisplay:
      return "Cathode Ray Tube Display";
    case icSigPMDisplay:
      return "Passive Matrix Display";
    case icSigAMDisplay:
      return "Active Matrix Display";
    case icSigPhotoCD:
      return "Photo CD";
    case icSigPhotoImageSetter:
      return "Photo Image Setter";
    case icSigGravure:
      return "Gravure";
    case icSigOffsetLithography:
      return "Offset Lithography";
    case icSigSilkscreen:
      return "Silk Screen";
    case icSigFlexography:
      return "Flexography";
    default:
      return "unknown";
  }
}

const char *
gcmmgetsequence (icProfile *profile, int nth)
{
  static char buf[BUFSIZ];
  char  *a1, *a2;
  int index = gcmmfindtag (profile, icSigProfileSequenceDescTag);
  icDescStruct *desc;

  if (index == -1)
    return false;

  desc = &(TAGPTR (icProfileSequenceDescType)->desc.data[nth]);

  if (desc->attributes & icTransparency)
    a1 = "transparency";
  else
    a1 = "reflective";

  if (desc->attributes & icMatte)
    a2 = "matte";
  else
    a2 = "glossy";

  sprintf (buf, "[%s,%s] %s: %s, %s (\"%s\", \"%s\")",
	   gcmmManufacturerInfo (desc->deviceMfg),
	   gcmmModelInfo (desc->deviceModel), strtechnology (desc->technology),
	   a1, a2, desc->deviceMfgDesc.desc, desc->modelDesc.desc);
  if (strlen (buf) > BUFSIZ)
    fprintf (stderr, "something bad will happen soon. touch me\n");

  return buf;
}

const char *
gcmmgettechnology (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigTechnologyTag);

  if (index == -1)
    return NULL;

  return strtechnology (TAGPTR (icSignatureType)->signature);
}

int
gcmmgetucrcount (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigUcrBgTag);

  if (index == -1)
    return false;

  return TAGPTR (icUcrBgType)->data.ucr.count;
}

unsigned short int *
gcmmgetucr (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigUcrBgTag);

  if (index == -1)
    return false;

  return TAGPTR (icUcrBgType)->data.ucr.curve;
}

int
gcmmgetbgcount (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigUcrBgTag);

  if (index == -1)
    return false;

  return TAGPTR (icUcrBgType)->data.bg.count;
}

unsigned short int *
gcmmgetbg (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigUcrBgTag);

  if (index == -1)
    return false;

  return TAGPTR (icUcrBgType)->data.bg.curve;
}

const char *
gcmmgetscreeningdesc (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigScreeningDescTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icTextDescriptionType)->desc.desc;
}

int
gcmmisprinterdefaultscreen (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigScreeningTag);

  if (index == -1)
    return -1;

  return TAGPTR (icScreeningType)->screen.screeningFlag
    & icPrtrDefaultScreensTrue;
}

int
gcmmsetprinterdefaultscreen (icProfile *profile, int val)
{
  int index = gcmmfindtag (profile, icSigScreeningTag);

  if (index == -1)
    return false;

  if (val)
    TAGPTR (icScreeningType)->screen.screeningFlag |= icPrtrDefaultScreensTrue;
  else
    TAGPTR (icScreeningType)->screen.screeningFlag
      &= ~icPrtrDefaultScreensTrue;

  return true;
}

int
gcmmislinesperinch (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigScreeningTag);

  if (index == -1)
    return -1;

  return TAGPTR (icScreeningType)->screen.screeningFlag
    & icLinesPerInch;
}

int
gcmmscreenchannels (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigScreeningTag);

  if (index == -1)
    return false;

  return TAGPTR (icScreeningType)->screen.channels;
}

double
gcmmscreenfrequency (icProfile *profile, int channel)
{
  int index = gcmmfindtag (profile, icSigScreeningTag);

  if (index == -1)
    return false;

  return gcmmds15f16 (TAGPTR (icScreeningType)->screen.data[channel].frequency);
}

double
gcmmscreenangle (icProfile *profile, int channel)
{
  int index = gcmmfindtag (profile, icSigScreeningTag);

  if (index == -1)
    return false;

  return gcmmds15f16 (TAGPTR (icScreeningType)->screen.data[channel].angle);
}

const char *
gcmmscreenshape (icProfile *profile, int channel)
{
  int index = gcmmfindtag (profile, icSigScreeningTag);

  if (index == -1)
    return NULL;

  switch (TAGPTR (icScreeningType)->screen.data[channel].spotShape)
  {
    case icSpotShapePrinterDefault:
      return "printer default";
    case icSpotShapeRound:
      return "round";
    case icSpotShapeDiamond:
      return "diamond";
    case icSpotShapeEllipse:
      return "ellipse";
    case icSpotShapeLine:
      return "line";
    case icSpotShapeSquare:
      return "square";
    case icSpotShapeCross:
      return "cross";
    default:
      return "unknown";
  }
}

const char *
gcmmgetviewingdesc (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigViewingCondDescTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icTextDescriptionType)->desc.desc;
}

double *
gcmmgetviewingilluminant (icProfile *profile)
{
  static double res[3];
  icXYZNumber *xyz;
  int index;

  index = gcmmfindtag (profile, icSigViewingConditionsTag);
  if (index == -1)
    return NULL;

  xyz = &(TAGPTR (icViewingConditionType)->view.illuminant);

  res[0] = gcmmds15f16 (xyz->X);
  res[1] = gcmmds15f16 (xyz->Y);
  res[2] = gcmmds15f16 (xyz->Z);

  return res;
}

double *
gcmmgetviewingsurround (icProfile *profile)
{
  static double res[3];
  icXYZNumber *xyz;
  int index;

  index = gcmmfindtag (profile, icSigViewingConditionsTag);
  if (index == -1)
    return NULL;

  xyz = &(TAGPTR (icViewingConditionType)->view.surround);

  res[0] = gcmmds15f16 (xyz->X);
  res[1] = gcmmds15f16 (xyz->Y);
  res[2] = gcmmds15f16 (xyz->Z);

  return res;
}

const char *
gcmmgetviewingstdilluminant (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigViewingConditionsTag);

  if (index == -1)
    return NULL;

  switch (TAGPTR (icViewingConditionType)->view.stdIlluminant)
  {
    case icIlluminantD50:
      return "D50";
    case icIlluminantD65:
      return "D65";
    case icIlluminantD93:
      return "D93";
    case icIlluminantF2:
      return "F2";
    case icIlluminantD55:
      return "D55";
    case icIlluminantA:
      return "A";
    case icIlluminantEquiPowerE:
      return "E";
    case icIlluminantF8:
      return "F8";
    default:
      return "unknown";
  }
}

const char *
gcmmpsname (icProfile *profile)
{
  int index = gcmmfindtag (profile, icSigCrdInfoTag);

  if (index == -1)
    return NULL;

  return TAGPTR (icCrdInfoType)->info.psname;
}

const char *
gcmmpscrdname (icProfile *profile, char *renderingintent)
{
  int index = gcmmfindtag (profile, icSigCrdInfoTag);

  if (index == -1)
    return NULL;

  switch (*renderingintent)
  {
    case 'p':
      return TAGPTR (icCrdInfoType)->info.rintent0;
    case 'r':
      return TAGPTR (icCrdInfoType)->info.rintent1;
    case 's':
      return TAGPTR (icCrdInfoType)->info.rintent2;
    case 'a':
      return TAGPTR (icCrdInfoType)->info.rintent3;
    default:
      return NULL;
  }
}

int
gcmmpscrdcount (icProfile *profile, char *renderingintent)
{
  int index;
  icSignature sig;

  switch (*renderingintent)
  {
    case 'p':
      sig = icSigPs2CRD0Tag;
    case 'r':
      sig = icSigPs2CRD1Tag;
    case 's':
      sig = icSigPs2CRD2Tag;
    case 'a':
      sig = icSigPs2CRD3Tag;
    default:
      return false;
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return false;

  return profile->tags.tag[index].size - 8;
}

int
gcmmisbinpscrd (icProfile *profile, char *renderingintent)
{
  int index;
  icSignature sig;

  switch (*renderingintent)
  {
    case 'p':
      sig = icSigPs2CRD0Tag;
    case 'r':
      sig = icSigPs2CRD1Tag;
    case 's':
      sig = icSigPs2CRD2Tag;
    case 'a':
      sig = icSigPs2CRD3Tag;
    default:
      return false;
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return false;

  return TAGPTR (icDataType)->data.dataFlag & icBinaryData;
}

unsigned char *
gcmmgetpscrd (icProfile *profile, char *renderingintent)
{
  int index;
  icSignature sig;

  switch (*renderingintent)
  {
    case 'p':
      sig = icSigPs2CRD0Tag;
    case 'r':
      sig = icSigPs2CRD1Tag;
    case 's':
      sig = icSigPs2CRD2Tag;
    case 'a':
      sig = icSigPs2CRD3Tag;
    default:
      return NULL;
  }

  index = gcmmfindtag (profile, sig);
  if (index == -1)
    return NULL;

  return TAGPTR (icDataType)->data.data;
}

unsigned char *
gcmmgetpscsa (icProfile *profile)
{
  int index;

  index = gcmmfindtag (profile, icSigPs2CSATag);
  if (index == -1)
    return NULL;

  return TAGPTR (icDataType)->data.data;
}

unsigned char *
gcmmgetpsrenderingintent (icProfile *profile)
{
  int index;

  index = gcmmfindtag (profile, icSigPs2RenderingIntentTag);
  if (index == -1)
    return NULL;

  return TAGPTR (icDataType)->data.data;
}

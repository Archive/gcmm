/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#ifndef __GCMMVALUE_H__
#define __GCMMVALUE_H__

/*
 * header things
 */
extern int gcmmhisembedded (icProfile *profile);
extern int gcmmhisuseanywhere (icProfile *profile);
extern int gcmmhisreflective (icProfile *profile);
extern int gcmmhistransparency (icProfile *profile);
extern int gcmmhisglossy (icProfile *profile);
extern int gcmmhismatte (icProfile *profile);
extern const char *gcmmhgetclass (icProfile *profile);
extern const char *gcmmhgetdevicespace (icProfile *profile);
extern const char *gcmmhgetconnectionspace (icProfile *profile);
extern const char *gcmmhgetprofiledate (icProfile *profile);
extern const char *gcmmhgetplatform (icProfile *profile);
extern const char *gcmmhgetmanufacturer (icProfile *profile);
extern const char *gcmmhgetmodel (icProfile *profile);
extern const char *gcmmhgetcmm (icProfile *profile);
extern const char *gcmmhgetrenderingintent (icProfile *profile);
extern double *gcmmhgetilluminant (icProfile *profile);

/*
 * Tag list things
 */
extern int gcmmisrequiredtag (icProfile *profile, icSignature sig);
extern int gcmmisprivatetag (icProfile *profile, icSignature sig);
extern unsigned long *gcmmgettags (icProfile *profile);
extern unsigned long *gcmmgetrequiredtags (icProfile *profile);
extern unsigned long *gcmmgetoptionaltags (icProfile *profile);

/*
 * things related to multi-dimensional lookup tables
 */
extern int gcmmgetluttype (icProfile *profile, const char *renderingintent,
			 int to_pcs);
extern int gcmmgetlutinputchannels (icProfile *profile, const char *
				  renderingintent, int to_pcs);
extern int gcmmgetlutoutputchannels (icProfile *profile, const char *
				    renderingintent, int to_pcs);
extern int gcmmgetlutgridpoints (icProfile *profile, const char *renderingintent,
			       int to_pcs);
extern int gcmmgetlutinputentries (icProfile *profile, const char *
				 renderingintent, int to_pcs);
extern int gcmmgetlutoutputentries (icProfile *profile, const char *
				  renderingintent, int to_pcs);
extern int gcmmgetclutsize (icProfile *profile, const char *renderingintent,
			  int to_pcs);
extern double *gcmmgetlutmatrix (icProfile *profile, const char *renderingintent,
			       int to_pcs);
extern void *gcmmgetlutinput (icProfile *profile, const char *
			    renderingintent, int to_pcs, int channel);
extern void *gcmmgetlutoutput (icProfile *profile, const char *
			     renderingintent, int to_pcs,int channel);
extern void *gcmmgetlutentry (icProfile *profile, const char *
			    renderingintent, int to_pcs, ...);

/*
 * Thing related to matrix/curve based tables
 */

extern double *gcmmgetcolorant (icProfile *profile, int channel);
extern int gcmmgetTRCcount (icProfile *profile, int channel);
extern unsigned short gcmmgetTRC (icProfile *profile, int channel, int nth);

/*
 * misc things
 */

extern const char *gcmmgetcalibrationdate (icProfile *profile);
extern const char *gcmmgetcharacterization (icProfile *profile);
extern const char *gcmmgetcopyright (icProfile *profile);
extern const char *gcmmgetmanufacturer (icProfile *profile);
extern const char *gcmmgetmodel (icProfile *profile);
extern double gcmmgetluminance (icProfile *profile);
extern const char *gcmmgetstdobserver (icProfile *profile);
extern double *gcmmgetbacking (icProfile *profile);
extern const char *gcmmgetgeometry (icProfile *profile);
extern const char *gcmmgetflare (icProfile *profile);
extern const char *gcmmgetilluminant (icProfile *profile);
extern double *gcmmgetblackpoint (icProfile *profile);
extern double *gcmmgetwhitepoint (icProfile *profile);
extern int gcmmgetcountnames (icProfile *profile);
extern const char *gcmmgetcolorprefix (icProfile *profile);
extern const char *gcmmgetcolorsuffix (icProfile *profile);
extern const char *gcmmgetcolorname (icProfile *profile, int nth);
extern double *gcmmgetcolorpcs (icProfile *profile, int nth);
extern double *gcmmgetcolordvc (icProfile *profile, int nth);
extern const char *gcmmgetprofiledescription (icProfile *profile);
extern int gcmmgetsequencecount (icProfile *profile);
extern const char *gcmmgetsequence (icProfile *profile, int nth);
extern const char *gcmmgettechnology (icProfile *profile);
extern int gcmmgetucrcount (icProfile *profile);
extern unsigned short int *gcmmgetucr (icProfile *profile);
extern int gcmmgetbgcount (icProfile *profile);
extern unsigned short int *gcmmgetbg (icProfile *profile);
extern const char *gcmmgetscreeningdesc (icProfile *profile);
extern int gcmmisprinterdefaultscreen (icProfile *profile);
extern int gcmmsetprinterdefaultscreen (icProfile *profile, int val);
extern int gcmmislinesperinch (icProfile *profile);
extern int gcmmscreenchannels (icProfile *profile);
extern double gcmmscreenfrequency (icProfile *profile, int channel);
extern double gcmmscreenangle (icProfile *profile, int channel);
extern const char *gcmmscreenshape (icProfile *profile, int channel);
extern const char *gcmmgetviewingdesc (icProfile *profile);
extern double *gcmmgetviewingilluminant (icProfile *profile);
extern double *gcmmgetviewingsurround (icProfile *profile);
extern const char *gcmmgetviewingstdilluminant (icProfile *profile);
extern const char *gcmmpsname (icProfile *profile);
extern const char *gcmmpscrdname (icProfile *profile, char *renderingintent);
extern int gcmmpscrdcount (icProfile *profile, char *renderingintent);
extern int gcmmisbinpscrd (icProfile *profile, char *renderingintent);
extern unsigned char *gcmmgetpscrd (icProfile *profile, char *renderingintent);
extern unsigned char *gcmmgetpscsa (icProfile *profile);
extern unsigned char *gcmmgetpsrenderingintent (icProfile *profile);

/*
 * Changing the world
 */

extern int gcmmhsetrenderingintent (icProfile *profile, const char *intent);
extern int gcmmsetlutmatrix (icProfile *profile, const char *renderingintent,
			   int to_pcs, double matrix[9]);
extern int gcmmsetlutinput (icProfile *profile, const char *renderingintent,
			  int to_pcs, int channel, unsigned short *array);
extern int gcmmsetlutoutput (icProfile *profile, const char *renderingintent,
			   int to_pcs, int channel, unsigned short *array);
extern int gcmmsetlutentry (icProfile *profile, const char *renderingintent,
			  int to_pcs, unsigned short *newarray, ...);
extern int gcmmsetcolorant (icProfile *profile, double *col, int channel);
extern int gcmmsetTRC (icProfile *profile, int to_pcs, int channel,
		     unsigned short val, int nth);

#endif

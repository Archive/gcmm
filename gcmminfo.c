/* gccm is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* gcmm - the Gimp Color Management Module
 * Copyright (C) 1998 ciccio, Olof Kylander, Raph Levien, and Jay Cox
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <getopt.h>
#include "gcmm.h"
#include "gcmmread.h"
#include "gcmmvalue.h"
#include "gcmmutil.h"

#define LINELEN            75
#define TAGPTR(type)       ((type *)profile->tags.tag[index].data)
#define TAGSIG             TAGPTR (icGenericType)->base.sig
#define SIG                profile->tags.tag[index].sig

#ifndef WORDS_BIGENDIAN
#define SIGCHARS sig.c.b3, sig.c.b2, sig.c.b1, sig.c.b0
#else
#define SIGCHARS sig.c.b0, sig.c.b1, sig.c.b2, sig.c.b3
#endif

typedef union Signature
{
  unsigned long val;
  struct
  {
    unsigned char b0;
    unsigned char b1;
    unsigned char b2;
    unsigned char b3;
  }c;
}SIGNATURE;

typedef struct Taglist
{
  struct Taglist *next;
  SIGNATURE sig;
  int index;
}TAGLIST;

static struct option long_options[] =
{
  {"all", 0, 0, 'a'},
  {"header", 0, 0, 'H'},
  {"help", 0, 0, 'h'},
  {"plot", 0, 0, 'p'},
  {"table", 0, 0, 't'},
  {"optional", 0, 0, 0},     /* index 5 */
  {"private", 0, 0, 0},
  {"required", 0, 0, 0},     /* index 7 */
  {"tag", 1, 0, 'n'},
  {"signature", 1, 0, 's'},
  {"summary", 0, 0, 'S'},
  {"version", 0, 0, 'v'},
  {0, 0, 0, 0}
};

static void usage (char *Prog);
static void version (void);
static void addtagsig (TAGLIST **taglist, unsigned long sig);
static void addtagindex (TAGLIST **taglist, int sig);
static void summary (icProfile *profile);
static unsigned long setval (icSignature Sig);
static void header (icProfile *profile, char *filename);
static void table (icProfile *profile, char *filename);
static void required (icProfile *profile, char *filename);
static void optional (icProfile *profile, char *filename);
static void private (icProfile *profile, char *filename);
static int countlist (TAGLIST *root);
static void dotag (icProfile *profile, TAGLIST *root, int plot);
static char *strsig (icSignature Sig);
static void showcurve (icCurve *curve, int plot);
static void showdata (icData *data, icUInt32Number size);
static void showtime (icDateTimeNumber *date);
static void showlut16 (icLut16 *lut, int plot);
static void showlut8 (icLut8 *lut, int plot);
static void showmeasurement (icProfile *profile);
static void showcolornames (icProfile *profile);
static void showsequence (icProfile *profile);
static void showsf32 (icS15Fixed16Array data, int size);
static void showscreen (icProfile *profile);
static void showsig (icSignature sig);
static void showtext (char *data, int size);
static void showuf32 (icU16Fixed16Array data, int size);
static void showucrbg (icProfile *profile, int plot);
static void showu16 (icUInt16Array data, int size);
static void showu32 (icUInt32Array data, int size);
static void showu64 (icUInt64Array data, int size);
static void showu8 (icUInt8Array data, int size);
static void showviewing (icProfile *profile);
static void showxyz (icXYZNumber *xyz);

void
main (int argc, char **argv)
{
  int c, option_index = 0, status = 0;
  int head=0, tab=0, tabopt=0, all=0, plot=0, tag=-1, sum=0;
  SIGNATURE sig;
  TAGLIST *taglist = NULL;
  icProfile *profile;

  if (argc <= 1)
    usage (argv[0]);

  sig.val = 0L;

  for (;;)
  {
    c = getopt_long (argc, argv, "SaHh?ptT:n:s:v", long_options,
		     &option_index);
    if (c == -1)
      break;

    switch (c)
    {
      case 0:
	tab = 1;
	if (option_index == 5)
	  tabopt = 'o';
	else if (option_index == 6)
	  tabopt = 'p';
	else if (option_index == 7)
	  tabopt = 'r';
	break;
      case 'a':
	all = 1;
	break;
      case 'H':
	head = 1;
	break;
      case 'h':
      case '?':
	usage (argv[0]);
      case 'p':
	plot = 1;
	break;
      case 'S':
	sum = 1;
	break;
      case 'T':
	tab = 1;
	switch (*optarg)
	{
	  case 'r':
	    tabopt = 'r';
	    break;
	  case 'o':
	    tabopt = 'o';
	    break;
	  case 'p':
	    tabopt = 'p';
	    break;
	}
	break;
      case 't':
	tab = 1;
	break;
      case 'n':
	tag = atoi (optarg);
	addtagindex (&taglist, tag);
	break;
      case 's':
	{
	  int end = 0;

	  if (optarg[0])
	    sig.c.b0 = optarg[0];
	  else
	  {
	    sig.c.b0 =' ';
	    end++;
	  }
	  if (!end && optarg[1])
	    sig.c.b1 = optarg[1];
	  else
	  {
	    sig.c.b1 =' ';
	    end++;
	  }
	  if (!end && optarg[2])
	    sig.c.b2 = optarg[2];
	  else
	  {
	    sig.c.b2 =' ';
	    end++;
	  }
	  if (!end && optarg[3])
	    sig.c.b3 = optarg[3];
	  else
	  {
	    sig.c.b3 =' ';
	    end++;
	  }
	}
	addtagsig (&taglist, sig.val);
	break;
      case 'v':
	version ();
      default:
	exit (1);
    }
  }

  if (optind >= argc)
  {
    printf ("missing profile name\n");
    exit (1);
  }

  while (optind < argc)
  {
    profile = gcmmread (argv[optind]);
    if (!profile)
    {
      fprintf (stderr, "Error reading %s\n", argv[optind]);
      optind++;
      status++;
      continue;
    }

    if (head + tab + all + (tag + 1) + sig.val == 0)
    {
      summary (profile);
      sum = 0;
    }
    else if (head + tab + (tag + 1) + sig.val == 0)
    {
      dotag (profile, NULL, plot);
      optind ++;
      continue;
    }

    if (sum)
      summary (profile);

    if (head)
      header (profile, argv[optind]);

    if (tab)
    {
      if (tabopt == 'r')
	required (profile, argv[optind]);
      else if (tabopt == 'p')
	private (profile, argv[optind]);
      else if (tabopt == 'o')
	optional (profile, argv[optind]);
      else
	table (profile, argv[optind]);
    }

    if (taglist)
      dotag (profile, taglist, plot);

    optind ++;
  }
  exit (status);
}

static void
usage (char *Prog)
{
  char *prog;

  prog = (prog = strrchr (Prog, '/')) != NULL ? prog + 1 : Prog;

  fprintf (stderr, "Usage:\n");
  fprintf (stderr, "%s [options] icc_profile\n", prog);
  fprintf (stderr, "Options:\n");
  fprintf (stderr, "<no option>      print summary and non exhaustive syntax "
	   "check\n");
  fprintf (stderr, "-h   --help      print this message and exit\n");
  fprintf (stderr, "-v   --version   print version info and exit\n");
  fprintf (stderr, "-H   --header    print all info in profile header\n");
  fprintf (stderr, "-t   --table     print table of tags in profile\n");
  fprintf (stderr, "-Tr  --required  print table of required tags\n");
  fprintf (stderr, "-To  --optional  print table of optional tags\n");
  fprintf (stderr, "-Tp  --private   print table of private tags\n");
  fprintf (stderr, "-s   --signature print tag with signature XXXX\n");
  fprintf (stderr, "-n N --tag       print Nth tag\n");
  fprintf (stderr, "-p   --plot      print array tags as GNUplot input\n");
  fprintf (stderr, "-a   --all       print ascii version of profile\n");

  exit (1);
}

static void
version (void)
{
  fprintf (stderr, "This is Gimp Color Management Module library version %s\n",
	   VERSION);
  exit (0);
}

static void
addtagsig (TAGLIST **taglist, unsigned long sig)
{
  TAGLIST *ptr;

  ptr = *taglist;
  if (!ptr)
    ptr = *taglist = (TAGLIST *) calloc ((size_t)1, sizeof (TAGLIST));
  else
  {
    while (ptr->next)
      ptr = ptr->next;
    ptr->next = (TAGLIST *) calloc ((size_t)1, sizeof (TAGLIST));
    ptr = ptr->next;
  }
  if (!ptr)
    exit (1);

  ptr->index = -1;
  ptr->sig.val = sig;
}

static void
addtagindex (TAGLIST **taglist, int sig)
{
  TAGLIST *ptr;

  ptr = *taglist;
  if (!ptr)
    ptr = *taglist = (TAGLIST *) calloc ((size_t)1, sizeof (TAGLIST));
  else
  {
    while (ptr->next)
      ptr = ptr->next;
    ptr->next = (TAGLIST *) calloc ((size_t)1, sizeof (TAGLIST));
    ptr = ptr->next;
  }

  if (!ptr)
    exit (1);

  ptr->index = sig;
}

static void
summary (icProfile *profile)
{
  printf ("ProfileDesc.:\t%s\n", gcmmgetprofiledescription (profile));
  printf ("Copyright:\t%s\n", gcmmgetcopyright (profile));
  printf ("H ProfileDate:\t%s\n", gcmmhgetprofiledate (profile));
  printf ("H Manufacturer:\t%s\n", gcmmhgetmanufacturer (profile));
  printf ("H Model:\t%s\n", gcmmhgetmodel (profile));
  printf ("H Platform:\t%s\n", gcmmhgetplatform (profile));
  printf ("H Pref. CMM:\t%s\n", gcmmhgetcmm (profile));
  printf ("H DeviceClass:\t%s\n", gcmmhgetclass (profile));
  printf ("H DeviceSpace:\t%s\n", gcmmhgetdevicespace (profile));
  printf ("H PCS:\t\t%s\n", gcmmhgetconnectionspace (profile));
  printf ("H Attributes:\t%s, %s\n", gcmmhisreflective (profile) ? "reflective"
	  : "transparency", gcmmhisglossy (profile) ? "glossy" : "matte");
  printf ("H Rendering:\t%s\n", gcmmhgetrenderingintent (profile));
}

static unsigned long
setval (icSignature Sig)
{
  SIGNATURE sig;

  sig.val = Sig;
  if (!sig.c.b0)
    sig.c.b0 = ' ';
  if (!sig.c.b1)
    sig.c.b1 = ' ';
  if (!sig.c.b2)
    sig.c.b2 = ' ';
  if (!sig.c.b3)
    sig.c.b3 = ' ';
  return sig.val;
}

static void
header (icProfile *profile, char *filename)
{
  SIGNATURE sig;
  double *xyz;

  printf ("# File name: %s\n", filename);
  printf ("# HEADER\n");
  printf ("Filesize: %d\n", profile->header.size);
  sig.val = setval(profile->header.cmmId);
  printf ("CMM: %c%c%c%c\n", SIGCHARS);
  printf ("Format: %s\n", gcmmstrbcd32 (profile->header.version));
  sig.val = setval (profile->header.colorSpace);
  printf ("ColorSpace: %c%c%c%c\n", SIGCHARS);
  sig.val = setval (profile->header.pcs);
  printf ("PCS: %c%c%c%c\n", SIGCHARS);
  printf ("Date: %d/%d/%d/%d/%d/%d\n", profile->header.date.year,
	  profile->header.date.month, profile->header.date.day,
	  profile->header.date.hours, profile->header.date.minutes,
	  profile->header.date.seconds);
  sig.val = setval (profile->header.magic);
  printf ("Magic: %c%c%c%c\n", SIGCHARS);
  sig.val = setval (profile->header.platform);
  printf ("Platform: %c%c%c%c\n", SIGCHARS);
  printf ("Flag1: %sembedded\n", gcmmhisembedded (profile) ? "" : "not ");
  printf ("Flag2: %suse anywhere\n", gcmmhisuseanywhere (profile) ? ""
	  : "don't ");
  sig.val = setval (profile->header.manufacturer);
  printf ("Manufacturer: %c%c%c%c\n", SIGCHARS);
  sig.val = setval (profile->header.model);
  printf ("Model: %c%c%c%c\n", SIGCHARS);
  printf ("Attribute1: %s\n", gcmmhisreflective (profile) ? "reflective"
	  : "transparency");
  printf ("Attribute2: %s\n", gcmmhisglossy (profile) ? "glossy" : "matte");
  printf ("Rendering: %s\n", gcmmhgetrenderingintent (profile));
  xyz = gcmmhgetilluminant (profile);
  printf ("Illuminant: (%f, %f, %f)\n", xyz[0], xyz[1], xyz[2]);

  sig.val = setval (profile->header.creator);
  printf ("Creator: %c%c%c%c\n", SIGCHARS);
}

static void
table (icProfile *profile, char *filename)
{
  SIGNATURE sig;
  int i;

  printf ("# File name: %s\n", filename);
  printf ("# TAGTABLE\n");

  for (i = 0; i < profile->tags.count; i++)
  {
    sig.val = setval (profile->tags.tag[i].sig);
    printf ("%2d:\t%c%c%c%c\t", i, SIGCHARS);
    sig.val
      = setval (((icGenericType *)(profile->tags.tag[i].data))->base.sig);
    printf ("%c%c%c%c\n", SIGCHARS);
  }
}

static void
required (icProfile *profile, char *filename)
{
  SIGNATURE sig;
  int i;

  printf ("# File name: %s\n", filename);
  printf ("# TAGTABLE\n");

  for (i = 0; i < profile->tags.count; i++)
  {
    sig.val = setval (profile->tags.tag[i].sig);
    if (gcmmisrequiredtag (profile, sig.val))
    {
      printf ("%2d:\t%c%c%c%c\t", i, SIGCHARS);
      sig.val
	= setval (((icGenericType *)(profile->tags.tag[i].data))->base.sig);
      printf ("%c%c%c%c\n", SIGCHARS);
    }
  }
}

static void
optional (icProfile *profile, char *filename)
{
  SIGNATURE sig;
  int i;

  printf ("# File name: %s\n", filename);
  printf ("# TAGTABLE\n");

  for (i = 0; i < profile->tags.count; i++)
  {
    sig.val = setval (profile->tags.tag[i].sig);
    if (!gcmmisrequiredtag (profile, sig.val)
	&& !gcmmisprivatetag (profile, sig.val))
    {
      printf ("%2d:\t%c%c%c%c\t", i, SIGCHARS);
      sig.val
	= setval (((icGenericType *)(profile->tags.tag[i].data))->base.sig);
      printf ("%c%c%c%c\n", SIGCHARS);
    }
  }
}

static void
private (icProfile *profile, char *filename)
{
  SIGNATURE sig;
  int i;

  printf ("# File name: %s\n", filename);
  printf ("# TAGTABLE\n");

  for (i = 0; i < profile->tags.count; i++)
  {
    sig.val = setval (profile->tags.tag[i].sig);
    if (gcmmisprivatetag (profile, sig.val))
    {
      printf ("%2d:\t%c%c%c%c\t", i, SIGCHARS);
      sig.val
	= setval (((icGenericType *)(profile->tags.tag[i].data))->base.sig);
      printf ("%c%c%c%c\n", SIGCHARS);
    }
  }
}

static int
countlist (TAGLIST *root)
{
  TAGLIST *ptr = root;
  int i = 0;

  while (ptr)
  {
    i++;
    ptr = ptr->next;
  }

  return i;
}

static void
dotag (icProfile *profile, TAGLIST *root, int plot)
{
  TAGLIST *ptr;
  int index, j, tagcount, listcount;

  tagcount = profile->tags.count;
  listcount = countlist (root);

  for (index = 0; index < tagcount; index++)
  {
    if (root)
    {
      ptr = root;
      for (j=0; ptr && j<listcount; j++)
      {
	if (ptr->index == index)
	  break;
	ptr = ptr->next;
      }
      if (!ptr)
	continue;

      if (j==listcount)
      {
	for (j=0; j<listcount; j++)
	{
	  if (ptr->sig.val == SIG)
	    break;
	  ptr = ptr->next;
	}
	if (j == listcount)
	  continue;
      }
    }

    printf ("TAG %s", strsig (SIG));
    printf (" TYPE %s\n", strsig (TAGSIG));
    switch (TAGSIG)
    {
      case icSigCurveType:
	showcurve (&(TAGPTR (icCurveType)->curve), plot);
	break;
      case icSigDataType:
	showdata (&(TAGPTR(icDataType)->data), profile->tags.tag[index].size);
	break;
      case icSigDateTimeType:
	showtime (&(TAGPTR (icDateTimeType)->date));
	break;
      case icSigLut16Type:
	showlut16 (&(TAGPTR (icLut16Type)->lut), plot);
	break;
      case icSigLut8Type:
	showlut8 (&(TAGPTR (icLut8Type)->lut), plot);
	break;
      case icSigMeasurementType:
	showmeasurement (profile);
	break;
      case icSigNamedColorType:
	showcolornames (profile);
	break;
      case icSigProfileSequenceDescType:
	showsequence (profile);
	break;
      case icSigS15Fixed16ArrayType:
	showsf32 (TAGPTR (icS15Fixed16ArrayType)->data,
		  profile->tags.tag[index].size);
	break;
      case icSigScreeningType:
	showscreen (profile);
	break;
      case icSigSignatureType:
	showsig (TAGPTR (icSignatureType)->signature);
	break;
      case icSigTextType:
	showtext (TAGPTR (icTextType)->data,
		  profile->tags.tag[index].size);
	break;
      case icSigTextDescriptionType:
	showtext (TAGPTR (icTextDescriptionType)->desc.desc,
		  TAGPTR (icTextDescriptionType)->desc.count);
	break;
      case icSigU16Fixed16ArrayType:
	showuf32 (TAGPTR (icU16Fixed16ArrayType)->data,
		  profile->tags.tag[index].size);
	break;
      case icSigUcrBgType:
	showucrbg (profile, plot);
	break;
      case icSigUInt16ArrayType:
	showu16 (TAGPTR(icUInt16ArrayType)->data,
		 profile->tags.tag[index].size);
	break;
      case icSigUInt32ArrayType:
	showu32 (TAGPTR(icUInt32ArrayType)->data,
		 profile->tags.tag[index].size);
	break;
      case icSigUInt64ArrayType:
	showu64 (TAGPTR(icUInt64ArrayType)->data,
		 profile->tags.tag[index].size);
	break;
      case icSigUInt8ArrayType:
	showu8 (TAGPTR(icUInt8ArrayType)->data, profile->tags.tag[index].size);
	break;
      case icSigViewingConditionsType:
	showviewing (profile);
	break;
      case icSigXYZArrayType:
	showxyz (TAGPTR (icXYZType)->data);
	break;
      case icSigNamedColor2Type:
	showcolornames (profile);
	break;
      case icSigCrdInfoType:
      default:
	fprintf (stderr, "Don't know how to show %dth tag\n", index);
    }
    printf ("END TAG %s\n", strsig (SIG));
  }
}

static char *
strsig (icSignature Sig)
{
  SIGNATURE sig;
  static char buf[5];

  sig.val = Sig;
  buf[0] = sig.c.b3;
  buf[1] = sig.c.b2;
  buf[2] = sig.c.b1;
  buf[3] = sig.c.b0;

  return buf;
}

static void
showcurve (icCurve *curve, int plot)
{
  int i, limit, pos = 0;
  char buf[BUFSIZ];

  if (plot)
    printf ("# START PLOT\n");

  limit = curve->count;
  for (i=0; i<limit; i++)
  {
    if (plot)
      printf ("%d %d\n", i, curve->data[i]);
    else
    {
      sprintf (buf, "%d", curve->data[i]);
      if (pos + strlen (buf) > LINELEN)
      {
	if (pos)
	  printf ("\n");
	printf (buf);
	pos = strlen (buf);
      }
      else
      {
	if (pos)
	{
	  printf (" ");
	  pos++;
	}
	printf (buf);
	pos += strlen (buf);
      }
    }
  }
  if (pos)
    printf ("\n");

  if (plot)
    printf ("# END PLOT");
}

static void
showdata (icData *data, icUInt32Number size)
{
  int i, pos = 0;
  char buf[BUFSIZ];

  size -= 12;
  printf ("DATATYPE %s\n", data->dataFlag ? "binary" : "ascii");

  for (i = 0; i < size; i++)
  {
    if (data->dataFlag)
    {
      sprintf (buf, "0x%02X\n", data->data[i]);
      if (pos + strlen (buf) > LINELEN)
      {
	printf (buf);
	pos = strlen (buf);
      }
      else
      {
	if (pos)
	{
	  printf (" ");
	  pos++;
	}
	printf (buf);
	pos += strlen (buf);
      }
    }
    else
      printf ("%c", data->data[i]);
  }
  if (!data->dataFlag || pos)
    printf ("\n");
}

static void
showtime (icDateTimeNumber *date)
{
  printf ("%d/%d/%d/%d/%d/%d\n", date->year, date->month, date->day,
	  date->hours, date->minutes, date->seconds);
}

static void
showlut16 (icLut16 *lut, int plot)
{
  icUInt16Number *table;
  int i, pos;
  char buf[BUFSIZ];

  printf ("INPUT CHANNELS %d\n", lut->inputChan);
  printf ("OUTPUT CHANNELS %d\n", lut->outputChan);

  printf ("MATRIX:\n");
  if (!plot)
  {
    printf ("%f\t%f\t%f\n", gcmmds15f16 (lut->e00), gcmmds15f16 (lut->e01),
	    gcmmds15f16 (lut->e02));
    printf ("%f\t%f\t%f\n", gcmmds15f16 (lut->e10), gcmmds15f16 (lut->e11),
	    gcmmds15f16 (lut->e12));
    printf ("%f\t%f\t%f\n", gcmmds15f16 (lut->e20), gcmmds15f16 (lut->e21),
	    gcmmds15f16 (lut->e22));
  }
  else
  {
    printf ("# START PLOT\n");
    printf ("# %f\t%f\t%f\n", gcmmds15f16 (lut->e00), gcmmds15f16 (lut->e01),
	    gcmmds15f16 (lut->e02));
    printf ("# %f\t%f\t%f\n", gcmmds15f16 (lut->e10), gcmmds15f16 (lut->e11),
	    gcmmds15f16 (lut->e12));
    printf ("# %f\t%f\t%f\n", gcmmds15f16 (lut->e20), gcmmds15f16 (lut->e21),
	    gcmmds15f16 (lut->e22));

    for (i=0; i<256; i++)
    {
      double val[3];

      val[0] = (double)((double)i * gcmmds15f16 (lut->e00)
			+ (double)i * gcmmds15f16 (lut->e01)
			+ (double)i * gcmmds15f16 (lut->e02));
      val[1] = (double)((double)i * gcmmds15f16 (lut->e10)
			+ (double)i * gcmmds15f16 (lut->e11)
			+ (double)i * gcmmds15f16 (lut->e12));
      val[2] = (double)((double)i * gcmmds15f16 (lut->e20)
			+ (double)i * gcmmds15f16 (lut->e21)
			+ (double)i * gcmmds15f16 (lut->e22));
      printf ("%d %f %f %f\n", i, val[0], val[1], val[2]);
    }
    printf ("# END PLOT\n");
  }

  printf ("INPUT TABLE:\n");
  if (plot)
    printf ("# START PLOT\n");
  pos = 0;
  for (i = 0; i < lut->outputEnt; i++)
  {
    if (plot)
      printf ("%d %d\n", i, lut->inputTable[i]);
    else
    {
      sprintf (buf, "%d", lut->inputTable[i]);
      if (pos + strlen (buf) > LINELEN)
      {
	if (pos)
	  printf ("\n");
	printf (buf);
	pos = strlen (buf);
      }
      else
      {
	if (pos)
	{
	  printf (" ");
	  pos++;
	}
	printf (buf);
	pos += strlen (buf);
      }
    }
  }
  if (!plot && pos)
    printf ("\n");
  if (plot)
    printf ("# END PLOT\n");

  printf ("MULTI TABLE:\n");
  if (plot)
    printf ("# START PLOT\n");
  {
    unsigned int total, coords[MAX_CHAN], k;

    total = pow (lut->clutPoints, lut->inputChan);
    table = lut->clutTable;

    for (i = 0; i < MAX_CHAN; i++)
      coords[i] = 0;

    for (i = 0; i < total; i++)
    {
      if (plot)
	printf ("%d ", i);
      for (k = 0; k < lut->inputChan; k++)
      {
	if (k)
	  printf (" %d", coords[k]);
	else
	  printf ("%d", coords[k]);
      }
      if (plot)
	printf (" ");
      else
	printf ("\t=>\t");

      for (k = 0; k < lut->outputChan; k++)
      {
	if (k)
	  printf (" %d", *table);
	else
	  printf ("%d", *table);
	table++;
      }
      printf ("\n");
      for (k = lut->inputChan - 1; k >= 0; k--)
      {
	coords[k]++;
	if (coords[k] >= lut->clutPoints)
	  coords[k] = 0;
	else
	  break;
      }
    }
  }
  if (plot)
    printf ("# END PLOT\n");

  printf ("OUTPUT TABLE:\n");
  if (plot)
    printf ("# START PLOT\n");
  pos = 0;
  for (i = 0; i < lut->outputEnt; i++)
  {
    if (plot)
      printf ("%d %d\n", i, lut->outputTable[i]);
    else
    {
      sprintf (buf, "%d", lut->outputTable[i]);
      if (pos + strlen (buf) > LINELEN)
      {
	if (pos)
	  printf ("\n");
	printf (buf);
	pos = strlen (buf);
      }
      else
      {
	if (pos)
	{
	  printf (" ");
	  pos++;
	}
	printf (buf);
	pos += strlen (buf);
      }
    }
  }
  if (!plot && pos)
    printf ("\n");
  if (plot)
    printf ("# END PLOT\n");
}

static void
showlut8 (icLut8 *lut, int plot)
{
  icUInt8Number *table;
  int c, i, pos = 0;
  char buf[BUFSIZ];

  printf ("INPUT CHANNELS %d\n", lut->inputChan);
  printf ("OUTPUT CHANNELS %d\n", lut->outputChan);

  printf ("MATRIX:\n");
  if (!plot)
  {
    printf ("%f\t%f\t%f\n", gcmmds15f16 (lut->e00), gcmmds15f16 (lut->e01),
	    gcmmds15f16 (lut->e02));
    printf ("%f\t%f\t%f\n", gcmmds15f16 (lut->e10), gcmmds15f16 (lut->e11),
	    gcmmds15f16 (lut->e12));
    printf ("%f\t%f\t%f\n", gcmmds15f16 (lut->e20), gcmmds15f16 (lut->e21),
	    gcmmds15f16 (lut->e22));
  }
  else
  {
    printf ("# START PLOT\n");
    printf ("# %f\t%f\t%f\n", gcmmds15f16 (lut->e00), gcmmds15f16 (lut->e01),
	    gcmmds15f16 (lut->e02));
    printf ("# %f\t%f\t%f\n", gcmmds15f16 (lut->e10), gcmmds15f16 (lut->e11),
	    gcmmds15f16 (lut->e12));
    printf ("# %f\t%f\t%f\n", gcmmds15f16 (lut->e20), gcmmds15f16 (lut->e21),
	    gcmmds15f16 (lut->e22));
    for (i=0; i<256; i++)
    {
      double val[3];

      val[0] = (double)((double)i * gcmmds15f16 (lut->e00)
			+ (double)i * gcmmds15f16 (lut->e01)
			+ (double)i * gcmmds15f16 (lut->e02));
      val[1] = (double)((double)i * gcmmds15f16 (lut->e10)
			+ (double)i * gcmmds15f16 (lut->e11)
			+ (double)i * gcmmds15f16 (lut->e12));
      val[2] = (double)((double)i * gcmmds15f16 (lut->e20)
			+ (double)i * gcmmds15f16 (lut->e21)
			+ (double)i * gcmmds15f16 (lut->e22));
      printf ("%d %f %f %f\n", i, val[0], val[1], val[2]);
    }
    printf ("# END PLOT\n");
  }

  for (c = 0; c < lut->inputChan; c++)
  {
    printf ("INPUT TABLE: Channel %d\n", c + 1);
    if (plot)
      printf ("# START PLOT\n");
    pos = 0;
    for (i = 0; i < 256; i++)
    {
      if (plot)
	printf ("%d %d\n", i, lut->inputTable[i]);
      else
      {
	sprintf (buf, "%d", lut->inputTable[i]);
	if (pos + strlen (buf) > LINELEN)
	{
	  if (pos)
	    printf ("\n");
	  printf (buf);
	  pos = strlen (buf);
	}
	else
	{
	  if (pos)
	  {
	    printf (" ");
	    pos++;
	  }
	  printf (buf);
	  pos += strlen (buf);
	}
      }
    }
  }
  if (!plot && pos)
    printf ("\n");
  if (plot)
    printf ("# END PLOT\n");

  printf ("MULTI TABLE:\n");
  if (plot)
    printf ("# START PLOT\n");
  {
    unsigned int total, coords[MAX_CHAN], k;

    total = pow (lut->clutPoints, lut->inputChan);
    table = lut->clutTable;

    for (i = 0; i < MAX_CHAN; i++)
      coords[i] = 0;

    for (i = 0; i < total; i++)
    {
      if (plot)
	printf ("%d ", i);
      for (k = 0; k < lut->inputChan; k++)
      {
	if (k)
	  printf (" %d", coords[k]);
	else
	  printf ("%d", coords[k]);
      }
      if (plot)
	printf (" ");
      else
	printf ("\t=>\t");

      for (k = 0; k < lut->outputChan; k++)
      {
	if (k)
	  printf (" %d", *table);
	else
	  printf ("%d", *table);
	table++;
      }
      printf ("\n");
      for (k = lut->inputChan - 1; k >= 0; k--)
      {
	coords[k]++;
	if (coords[k] >= lut->clutPoints)
	  coords[k] = 0;
	else
	  break;
      }
    }
  }
  if (plot)
    printf ("# END PLOT\n");

  for (c = 0; c < lut->outputChan; c++)
  {
    printf ("OUTPUT TABLE: Channel %d\n", c + 1);
    if (plot)
      printf ("# START PLOT\n");
    pos = 0;
    for (i = 0; i < 256; i++)
    {
      if (plot)
	printf ("%d %d\n", i, lut->outputTable[i]);
      else
      {
	sprintf (buf, "%d", lut->outputTable[i]);
	if (pos + strlen (buf) > LINELEN)
	{
	  if (pos)
	    printf ("\n");
	  printf (buf);
	  pos = strlen (buf);
	}
	else
	{
	  if (pos)
	  {
	    printf (" ");
	    pos++;
	  }
	  printf (buf);
	  pos += strlen (buf);
	}
      }
    }
  }
  if (!plot && pos)
    printf ("\n");
  if (plot)
    printf ("# END PLOT\n");
}

static void
showmeasurement (icProfile *profile)
{
  double *back;

  printf ("OBSERVER %s\n", gcmmgetstdobserver (profile));
  back = gcmmgetbacking (profile);
  printf ("BACKING %f,%f,%f\n", back[0], back[1], back[2]);
  printf ("GEOMETRY %s\n", gcmmgetgeometry (profile));
  printf ("FLARE %s\n", gcmmgetflare (profile));
  printf ("ILLUMINANT %s\n", gcmmgetilluminant (profile));
}

static void
showcolornames (icProfile *profile)
{
  double *co;
  int i, count = gcmmgetcountnames (profile);

  if (!count)
    return;
  printf ("PREFIX %s\n", gcmmgetcolorprefix (profile));
  printf ("SUFFIX %s\n", gcmmgetcolorsuffix (profile));

  for (i = 0; i < count; i++)
  {
    printf ("COLOR %s", gcmmgetcolorname (profile, i));
    co = gcmmgetcolorpcs (profile, i);
    printf (" (%f,%f,%f)", co[0], co[1], co[2]);
    co = gcmmgetcolordvc (profile, i);
    if (!(fabs (co[0]) == 0.0 && fabs (co[1]) == 0.0 && fabs (co[2]) == 0.0))
      printf (" (%f,%f,%f)", co[0], co[1], co[2]);
    printf ("\n");
  }
}

static void
showsequence (icProfile *profile)
{
  int i, count = gcmmgetsequencecount (profile);

  for (i = 0; i < count; i++)
    printf ("SEQ %s\n", gcmmgetsequence (profile, i));
}

static void
showsf32 (icS15Fixed16Array data, int size)
{
  int i, pos = 0;
  char buf[BUFSIZ];

  for (i = 0; i < size; i++)
  {
    sprintf (buf, "%f", gcmmds15f16 (data[i]));
    if (pos + strlen (buf) > LINELEN)
    {
      printf (buf);
      pos = strlen (buf);
    }
    else
    {
      if (pos)
      {
	printf (" ");
	pos++;
      }
      printf (buf);
      pos += strlen (buf);
    }
  }
  if (pos)
    printf ("\n");
}

static void
showscreen (icProfile *profile)
{
  int i, chan = gcmmscreenchannels (profile);

  printf ("PRINTERDEFAULT %d\n", gcmmisprinterdefaultscreen (profile));
  printf ("FREQUENCYUNIT lines/%s\n", gcmmislinesperinch (profile)
	  ? "in" : "cm");

  printf ("FREQUENCY");
  for (i = 0; i < chan; i++)
    printf ("\t%f", gcmmscreenfrequency (profile, i));
  printf ("\n");
  printf ("SCREENANGLE");
  for (i = 0; i < chan; i++)
    printf ("\t%f", gcmmscreenangle (profile, i));
  printf ("\n");
  printf ("SPOTSHAPE");
  for (i = 0; i < chan; i++)
    printf ("\t%s", gcmmscreenshape (profile, i));
  printf ("\n");
}

static void
showsig (icSignature sig)
{
  printf ("SIGNATURE %s\n", strsig (sig));
}

static void
showtext (char *data, int size)
{
  char *buf, *ptr;

  buf = malloc (size + 1);
  if (!buf)
    return;

  memcpy (buf, data, size);
  ptr = buf;
  ptr[size] = '\0';
  while (*ptr)
  {
    if (*ptr == '\r')
      *ptr = '\n';
    ptr++;
  }
  printf ("TEXT %s\n", buf);
  free (buf);
}

static void
showuf32 (icU16Fixed16Array data, int size)
{
  int i, pos = 0;
  char buf[BUFSIZ];

  for (i = 0; i < size; i++)
  {
    sprintf (buf, "%f", gcmmdu16f16 (data[i]));
    if (pos + strlen (buf) > LINELEN)
    {
      printf (buf);
      pos = strlen (buf);
    }
    else
    {
      if (pos)
      {
	printf (" ");
	pos++;
      }
      printf (buf);
      pos += strlen (buf);
    }
  }
  if (pos)
    printf ("\n");
}

static void
showucrbg (icProfile *profile, int plot)
{
  int i, limit, pos;
  char buf[BUFSIZ];
  unsigned short *ptr;

  printf ("UNDER COLOR REMOVAL:\n");
  if (plot)
    printf ("# START PLOT\n");

  limit = gcmmgetucrcount (profile);
  ptr = gcmmgetucr (profile);
  pos = 0;
  for (i=0; i<limit; i++)
  {
    if (plot)
      printf ("%d %d\n", i, ptr[i]);
    else
    {
      sprintf (buf, "%d", ptr[i]);
      if (pos + strlen (buf) > LINELEN)
      {
	printf (buf);
	pos = strlen (buf);
      }
      else
      {
	if (pos)
	{
	  printf (" ");
	  pos++;
	}
	printf (buf);
	pos += strlen (buf);
      }
    }
  }
  if (pos)
    printf ("\n");

  if (plot)
    printf ("# END PLOT");

  printf ("BLACK GENERATION:\n");
  if (plot)
    printf ("# START PLOT\n");

  limit = gcmmgetbgcount (profile);
  ptr = gcmmgetbg (profile);
  pos = 0;
  for (i=0; i<limit; i++)
  {
    if (plot)
      printf ("%d %d\n", i, ptr[i]);
    else
    {
      sprintf (buf, "%d", ptr[i]);
      if (pos + strlen (buf) > LINELEN)
      {
	printf (buf);
	pos = strlen (buf);
      }
      else
      {
	if (pos)
	{
	  printf (" ");
	  pos++;
	}
	printf (buf);
	pos += strlen (buf);
      }
    }
  }
  if (pos)
    printf ("\n");

  if (plot)
    printf ("# END PLOT");
}

static void
showu16 (icUInt16Array data, int size)
{
  int i, pos = 0;
  char buf[BUFSIZ];

  for (i = 0; i < size; i++)
  {
    sprintf (buf, "0x%x", data[i]);
    if (pos + strlen (buf) > LINELEN)
    {
      printf (buf);
      pos = strlen (buf);
    }
    else
    {
      if (pos)
      {
	printf (" ");
	pos++;
      }
      printf (buf);
      pos += strlen (buf);
    }
  }
  if (pos)
    printf ("\n");
}

static void
showu32 (icUInt32Array data, int size)
{
  int i, pos = 0;
  char buf[BUFSIZ];

  for (i = 0; i < size; i++)
  {
    sprintf (buf, "0x%x", data[i]);
    if (pos + strlen (buf) > LINELEN)
    {
      printf ("\n");
      printf (buf);
      pos = strlen (buf);
    }
    else
    {
      if (pos)
      {
	printf (" ");
	pos++;
      }
      printf (buf);
      pos += strlen (buf);
    }
  }
  if (pos)
    printf ("\n");
}

static void
showu64 (icUInt64Array data, int size)
{
  int i, pos = 0;
  char buf[BUFSIZ];

  for (i = 0; i < size; i++)
  {
    sprintf (buf, "0x%Lx", data[i]);
    if (pos + strlen (buf) > LINELEN)
    {
      printf (buf);
      pos = strlen (buf);
    }
    else
    {
      if (pos)
      {
	printf (" ");
	pos++;
      }
      printf (buf);
      pos += strlen (buf);
    }
  }
  if (pos)
    printf ("\n");
}

static void
showu8 (icUInt8Array data, int size)
{
  int i, pos = 0;
  char buf[BUFSIZ];

  for (i = 0; i < size; i++)
  {
    sprintf (buf, "0x%x", data[i]);
    if (pos + strlen (buf) > LINELEN)
    {
      printf (buf);
      pos = strlen (buf);
    }
    else
    {
      if (pos)
      {
	printf (" ");
	pos++;
      }
      printf (buf);
      pos += strlen (buf);
    }
  }
  if (pos)
    printf ("\n");
}

static void
showviewing (icProfile *profile)
{
  double *val;

  val = gcmmgetviewingilluminant (profile);
  printf ("ILLUMINANT %f,%f,%f\n", val[0], val[1], val[2]);

  val = gcmmgetviewingsurround (profile);
  printf ("SURROUND %f,%f,%f\n", val[0], val[1], val[2]);

  printf ("STDILLUMINANT %s\n", gcmmgetviewingstdilluminant (profile));
}

static void
showxyz (icXYZNumber *xyz)
{
  printf ("XYZ (%f, %f, %f)\n", gcmmds15f16 (xyz->X), gcmmds15f16 (xyz->Y),
	  gcmmds15f16 (xyz->Z));
}
